﻿#if UNITY_IOS

using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.XCodeEditor;

namespace UnityEditor.TreggiOS
{
    public static class XCodePostProcess
    {
        [PostProcessBuild(317)] //random number to avoid post-process collisions
        public static void OnPostProcessBuild(BuildTarget target, string path)
        {
            if (target == BuildTarget.iPhone)
            {
				        //obtaining XCode project reference
                XCodeEditor.XCProject project = new XCodeEditor.XCProject(path);
                string projModPath = System.IO.Path.Combine(Application.dataPath, "tregg_unity/Editor");

				if(Directory.Exists(projModPath)) {
	                var files = System.IO.Directory.GetFiles(projModPath, "*.projmods", System.IO.SearchOption.AllDirectories);
	                foreach (var file in files)
	                {
	                    project.ApplyMod(Application.dataPath, file);
	                }
				}

				//consolidate (?) project and save file
				project.Consolidate();
				project.Save();

                //HACK to search under a second path
                projModPath = System.IO.Path.Combine(Application.dataPath, "Scripts/Library/tregg_unity/Editor");
				if(Directory.Exists(projModPath)) {
	                var files = System.IO.Directory.GetFiles(projModPath, "*.projmods", System.IO.SearchOption.AllDirectories);
	                foreach (var file in files)
	                {
	                    project.ApplyMod(Application.dataPath, file);
	                }
				}
                //consolidate (?) project and save file
                project.Consolidate();
                project.Save();
            }
        }
    }
}

#endif
