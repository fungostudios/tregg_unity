﻿using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Tregg
{
	namespace EditorExtension
	{
	
		#if UNITY_EDITOR
		[InitializeOnLoad]
		#endif
		public class TreggSettingsWindow : EditorWindow
		{
		
			#region Menu items
		    [MenuItem("Tregg/Settings")]
		    public static void ShowSettingsWindow()
		    {
		        EditorWindow.GetWindow<TreggSettingsWindow>(true, "Tregg Settings", true);
		    }
		
			#endregion
		
		    #region Tregg Settings
			
		    private string m_url;
			private string m_clientID;
			
			private string m_lastUrl;
			private string m_lastClientID;
			
			//default constructor, init class members with Tregg settings
			TreggSettingsWindow()
			{
				TreggSingleton instance = TreggSingleton.Instance;
				
				m_lastUrl = m_url = instance.URL;
				m_lastClientID = m_clientID = instance.ClientID;
			}
			
			#endregion
			
			#region window definition
			
			void OnGUI () 
			{
				GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
				m_url = EditorGUILayout.TextField ("Tregg URL", m_url);
				m_clientID = EditorGUILayout.TextField ("Client ID", m_clientID);
		
			}	
			
			void OnInspectorUpdate()
			{
				UpdateInstance();
			}
			
			void OnDestroy()
			{
				UpdateInstance();
			}
			
			private void UpdateInstance()
			{
				if(m_lastUrl != m_url || m_lastClientID != m_clientID)
				{
					TreggSingleton instance = TreggSingleton.Instance;
					
					instance.URL = m_lastUrl = m_url;
					instance.ClientID = m_lastClientID = m_clientID;
					
					instance.Synchronize();
				}
			}
			
			#endregion
		
		}
	}
}