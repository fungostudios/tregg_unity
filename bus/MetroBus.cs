#if UNITY_METRO

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tregg
{
    sealed class MetroBus : MonoBehaviour, IBus
    {
        private Assembly[] asms;
        private Dictionary<string, Type> types = new Dictionary<string, Type>(); 

        public void Init() {
            string[] dlls = {"Tregg"};
            List <Assembly> loadedDlls = new List<Assembly>();
            foreach (string dll in dlls) {
                loadedDlls.Add(Assembly.Load(new System.Reflection.AssemblyName(dll)));
            }

            asms = loadedDlls.ToArray();
        }


        private KeyValuePair<Type, Assembly> GetTypeFor(string className) {
            Type klass;
            if (types.TryGetValue(className, out klass))
            {
                return new KeyValuePair<Type,Assembly>(klass, klass.Assembly);
            }
            else
            {
                foreach (Assembly asm in asms)
                {
                    Type t = asm.GetType(className);

                    /*
                    try
                    {
                        bool fake_assingnment = t.IsClass; //HACK we cannot use Equals or == on Metro, we use the try/catch block as workaround
                        types[className] = klass;
                        return klass;
                    }
                    catch
                    {
                        //DO Nothing
                    }
                     */

                    if (((object)t) != ((object)null))
                    {
                        types[className] = klass = t;
                        return new KeyValuePair<Type,Assembly>(klass, asm);
                    }
                }
            }
            throw new Exception("Class " + className + " Not Found.");
        }

        private string CapitalizeMethodName(string methodName) {
            char[] methodChars = methodName.ToCharArray();
            methodChars[0] = char.ToUpper(methodChars[0]);
            return new string(methodChars);
        }

        public void CallMethod(string className, string methodName, string[] methodParams, int cid) {
            KeyValuePair<Type, Assembly> klassAsmPair = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            // types variable removed, because getMethod doesn't find the method if types are provided
            //MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);
            MethodInfo m = BusUtils.BusUtils.GetMethodInfo(klassAsmPair.Key, klassAsmPair.Value, CapitalizeMethodName(methodName), types);

            object resultObj = m.Invoke(klassAsmPair.Key, methodParams);
            Dictionary<string, object> result = resultObj != null ? resultObj as Dictionary<string, object> : new Dictionary<string, object>();
            result.Add(Bus.CallbackIdKey, cid.ToString());
            Bus.Callback(result);
        }

        public void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid) {
            KeyValuePair<Type, Assembly> klassAsmPair = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen + 1];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            types[mLen] = typeof(Action<Dictionary<string, object>>);

            object[] mParams = new object[mLen + 1];
            methodParams.CopyTo(mParams, 0);
            mParams[mLen] = Bus.callbacks[cid];
            Bus.callbacks.Remove(cid);

            // types variable removed, because getMethod doesn't find the method if types are provided
            //MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);    Unsupported call in Metro environment
            MethodInfo m = BusUtils.BusUtils.GetMethodInfo(klassAsmPair.Key, klassAsmPair.Value, CapitalizeMethodName(methodName), types);
            m.Invoke(klassAsmPair.Key, mParams);
        }

        public void Callback(string message) {
            throw new Exception("Don't call me again!");
        }
    }
}

#endif