#if UNITY_ANDROID
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tregg
{
    sealed class AndroidBus : MonoBehaviour, IBus
    {
        private const string JAVA_BUS_CLASS_NAME = "com.fungostudios.unitybus.Bus";
        private const string JAVA_TREGG_PACKAGE_NAME = "com.fungostudios.tregg";
        
		#if BUS_EDITOR_OVERRIDE
		private static Assembly s_currentAsm = null;
		private static Assembly CurrentAssembly
		{
			get
			{
				if(s_currentAsm ==  null)
				{
					s_currentAsm = Assembly.GetExecutingAssembly();
					if(s_currentAsm != null)
					{
						Debug.Log("Executing assembly reference initialized successfully");
					}
					else
					{
						Debug.Log("Executing assembly reference initialization failed");
					}
				}
				
				return s_currentAsm;
			}
		}
		#endif
		

#if UNITY_ANDROID
        private AndroidJavaClass busJava;
        private AndroidJavaClass BusJava
        {
            get
            {
                if (busJava == null)
                {
                    Debug.Log("Creating new BusJava instance");
                    busJava = new AndroidJavaClass(JAVA_BUS_CLASS_NAME);

                    if (busJava == null)
                    {
                        throw new MissingReferenceException(string.Format("AndroidBus failed to load {0} class", JAVA_BUS_CLASS_NAME));
                    }
                    else
                    {
						Debug.Log("JavaBus created successfully");
					}
                }
                return busJava;
            }
        }
#endif

		

        public void Init() 
        {
			/*

#if UNITY_ANDROID
            BusJava.CallStatic("init");
            Bus.CallMethod("TreggConfigBus", "init", new string[] { TreggSingleton.Instance.URL, TreggSingleton.Instance.ClientID }, (Dictionary<string, object> result) =>
            {
                Debug.Log("Tregg init command sent.");
            });
#endif
            */
        }

        private string uncapitalizeMethodName(string methodName)
        {
            short firstLetter = (short)methodName.ToCharArray()[0];

            if (firstLetter >= 'A')
            {
                firstLetter -= ('A' - 'a');
                string ret = (char)firstLetter + methodName.Substring(1);

                Debug.Log("Uncapitalized " + methodName + " to " + ret);
                return ret;
            }
            else
            {
                return methodName;
            }            
        }
        
		#if BUS_EDITOR_OVERRIDE
		private const string DEFAULT_TREGG_NAMESPACE = "Tregg";
		private KeyValuePair<Type, MethodInfo> GetMethodInfos(string className, string methodName, int nParams, bool appendCallback) 
		{
		
			MethodInfo methodInfo = null;
			className = DEFAULT_TREGG_NAMESPACE + "." + className;
			Type classType = CurrentAssembly.GetType(className);
			if(classType != null)
			{
				int arrLen = appendCallback ? nParams + 1 : nParams;
				Type[] paramTypes = new Type[arrLen];
				for(int i = 0; i < nParams; ++i)
				{
					paramTypes[i] = typeof(string);
				}
				
				if(appendCallback)
				{
					paramTypes[nParams] = typeof(Action<Dictionary<string, object>>);
				}
				
				char[] methodNameChars = methodName.ToCharArray();
				methodNameChars[0] = char.ToUpper(methodNameChars[0]);
				methodName = new string(methodNameChars);
				
				methodInfo = classType.GetMethod(methodName, paramTypes);
			}
			
			KeyValuePair<Type, MethodInfo> ret = new KeyValuePair<Type, MethodInfo>(classType, methodInfo);
			
			Debug.Log("Looked for "+className+"."+methodName+" and found <"+(classType != null ? classType.ToString() : "NULL")+","+(methodInfo != null ? methodInfo.ToString() : "NULL")+">");
			
			return ret;
		}
		
		#endif
		

        public void CallMethod(string className, string methodName, string[] methodParams, int cid) {
#if UNITY_ANDROID
			#if BUS_EDITOR_OVERRIDE
			KeyValuePair<Type, MethodInfo> classMethodPair = GetMethodInfos(className, methodName, methodParams.Length, false);
			if(classMethodPair.Value != null)
			{
				Dictionary<string, object> result = classMethodPair.Value.Invoke(classMethodPair.Key, methodParams) as Dictionary<string, object>;
				if (result == null) //this will happen if m returns void, which is legal
				{
					result = new Dictionary<string, object>();
				}
				result.Add(Bus.CallbackIdKey, cid.ToString());
				Bus.Callback(result);
				return;
			}
			#endif
            string jsonParams = MiniJSON.Json.Serialize(methodParams);
            object[] mParams = { JAVA_TREGG_PACKAGE_NAME + "." + className, methodName, jsonParams, cid.ToString() };

            Debug.Log("Calling " + className + "." + methodName + " with BusJava(" + busJava + ")");

            BusJava.CallStatic("callMethod", mParams);
#endif 
        }

        public void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid) {
#if UNITY_ANDROID
			#if BUS_EDITOR_OVERRIDE
			KeyValuePair<Type, MethodInfo> classMethodPair = GetMethodInfos(className, methodName, methodParams.Length, true);
			if(classMethodPair.Value != null)
			{
				object[] tmp = new object[methodParams.Length + 1];
				methodParams.CopyTo(methodParams, 0);
				tmp[methodParams.Length] = Bus.callbacks[cid];
				Bus.callbacks.Remove(cid);

				classMethodPair.Value.Invoke(classMethodPair.Key, tmp );
				return;
			}
			#endif
			string jsonParams = MiniJSON.Json.Serialize(methodParams);
            object[] mParams = { JAVA_TREGG_PACKAGE_NAME + "." + className, methodName, jsonParams, cid.ToString() };

			Debug.Log("Calling " + className + "." + methodName + " with BusJava(" + busJava + ")");
			
			BusJava.CallStatic("callAsyncMethod", mParams);
#endif
        }

        public void Callback(string message) {
            Bus.Callback(message);
        }
    }
}
   
#endif