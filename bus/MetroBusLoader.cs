using UnityEngine;
using System.Collections;

#if UNITY_METRO

namespace Tregg
{
    public class MetroBusLoader : Bus.CompiledBusLoader
    {
        protected override IBus bus
        {
            get
            {
                return BusComponentFactory.GetComponent<MetroBus>();
            }
        }
    }
}

#endif