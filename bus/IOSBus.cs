#if UNITY_IPHONE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

#if BUS_EDITOR_OVERRIDE
using System.Reflection;
#endif

namespace Tregg
{
    sealed class IOSBus : MonoBehaviour, IBus
    {

#if BUS_EDITOR_OVERRIDE

        private static Assembly s_currentAsm = null;
        private static Assembly CurrentAssembly
        {
            get
            {
                if (s_currentAsm == null)
                {
                    s_currentAsm = Assembly.GetExecutingAssembly();
                    if (s_currentAsm != null)
                    {
                        Debug.Log("Executing assembly reference initialized successfully");
                    }
                    else
                    {
                        Debug.Log("Executing assembly reference initialization failed");
                    }
                }

                return s_currentAsm;
            }
        }

        private const string DEFAULT_TREGG_NAMESPACE = "Tregg";
        private KeyValuePair<Type, MethodInfo> GetMethodInfos(string className, string methodName, int nParams, bool appendCallback)
        {

            MethodInfo methodInfo = null;
            className = DEFAULT_TREGG_NAMESPACE + "." + className;
            Type classType = CurrentAssembly.GetType(className);
            if (classType != null)
            {
                int arrLen = appendCallback ? nParams + 1 : nParams;
                Type[] paramTypes = new Type[arrLen];
                for (int i = 0; i < nParams; ++i)
                {
                    paramTypes[i] = typeof(string);
                }

                if (appendCallback)
                {
                    paramTypes[nParams] = typeof(Action<Dictionary<string, object>>);
                }

                char[] methodNameChars = methodName.ToCharArray();
                methodNameChars[0] = char.ToUpper(methodNameChars[0]);
                methodName = new string(methodNameChars);

                methodInfo = classType.GetMethod(methodName, paramTypes);
            }

            KeyValuePair<Type, MethodInfo> ret = new KeyValuePair<Type, MethodInfo>(classType, methodInfo);

            Debug.Log("Looked for " + className + "." + methodName + " and found <" + (classType != null ? classType.ToString() : "NULL") + "," + (methodInfo != null ? methodInfo.ToString() : "NULL") + ">");

            return ret;
        }

#endif

          [DllImport ("__Internal")]
		      private static extern void _Init();

          [DllImport ("__Internal")]
          private static extern void _CallMethod(string className, string methodName, string methodParams, int cid);

          [DllImport ("__Internal")]
          private static extern void _CallAsyncMethod(string className, string methodName, string methodParams, int cid);


		      public void Init()
          {
            _Init();
          }
	
          public void CallMethod(string className, string methodName, string[] methodParams, int cid)
          {
#if BUS_EDITOR_OVERRIDE
              KeyValuePair<Type, MethodInfo> classMethodPair = GetMethodInfos(className, methodName, methodParams.Length, false);
              if (classMethodPair.Value != null)
              {
                  Dictionary<string, object> result = classMethodPair.Value.Invoke(classMethodPair.Key, methodParams) as Dictionary<string, object>;
                  if (result == null) //this will happen if m returns void, which is legal
                  {
                      result = new Dictionary<string, object>();
                  }
                  result.Add(Bus.CallbackIdKey, cid.ToString());
                  Bus.Callback(result);
                  return;
              }
    #endif
              string jsonParams = MiniJSON.Json.Serialize(methodParams);
            _CallMethod(className, methodName, jsonParams, cid);
          }

          public void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid)
          {
#if BUS_EDITOR_OVERRIDE
              KeyValuePair<Type, MethodInfo> classMethodPair = GetMethodInfos(className, methodName, methodParams.Length, true);
              if (classMethodPair.Value != null)
              {
                  object[] tmp = new object[methodParams.Length + 1];
                  methodParams.CopyTo(methodParams, 0);
                  tmp[methodParams.Length] = Bus.callbacks[cid];
                  Bus.callbacks.Remove(cid);

                  classMethodPair.Value.Invoke(classMethodPair.Key, tmp);
                  return;
              }
    #endif
            string jsonParams = MiniJSON.Json.Serialize(methodParams);
            _CallAsyncMethod(className, methodName, jsonParams, cid);
          }

          public void Callback(string message) 
          {
            Bus.Callback(message);
          }
	  }
}
#endif
