﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Tregg;

namespace Tregg
{
	public class TreggSingleton
	{
		#region Singleton accessor
		private static TreggSingleton m_treggSingleton = null;
		
		public static TreggSingleton Instance
		{
			get
			{
				if(m_treggSingleton == null)
				{
					m_treggSingleton = new TreggSingleton();
				}
				return m_treggSingleton;
			}
		}
		
		#endregion
		
		#region Tregg serializable attributes
		
		//base url	
		private string m_baseUrl;
        private const string URL_DEFAULT_VALUE = "http://dd-staging-4mvbrmeibk.elasticbeanstalk.com";
        private const string URL_DICT_KEY = "URL";
	
		//client ID
		private string m_clientID;
        private const string CLIENT_ID_DEFAULT_VALUE = "nuybwsjfgh8njot5c317x8mii7b9p41";
		private const string CLIENT_ID_DICT_KEY = "CLIENT_ID";
	
		#endregion
			
		#region CTor, serialization and deserialization methods
		
		private string JSON_FILE_PATH;
		
		private void SerializeToJsonFile(string jsonFilePath)
		{
			//Debug.Log("Serializing tregg info to " + jsonFilePath);
			//create dictionary to serialize
			Dictionary<string, string> jsonData = new Dictionary<string, string>();
			
			//insert URL information
			jsonData[URL_DICT_KEY] = m_baseUrl;
			jsonData[CLIENT_ID_DICT_KEY] = m_clientID;

            /*
            {
                //encode & serialize dictionary
                StreamWriter sw = new StreamWriter(jsonFilePath);
                sw.WriteLine(MiniJSON.Json.Serialize(jsonData));
                sw.Flush();
                sw.Close();
             */ 
		}
		
		private bool DeserializeFromJsonFile(string jsonFilePath)
		{
            return false;
            /*
			//check file existance
			if(!File.Exists(jsonFilePath))
				return false;

            Dictionary<string, object> jsonData = null;

            try
            {
                //Debug.Log("Deserializing tregg info from " + jsonFilePath);
                //read json from file
                StreamReader sr = new StreamReader(jsonFilePath);
                string jsonContent = sr.ReadToEnd();
                sr.Close();
                //decode json
                //Debug.Log("Decoding this stuff:\n"+jsonContent);
                var jsonObject = MiniJSON.Json.Deserialize(jsonContent);
                //Debug.Log("Obtained " + jsonObject);
                jsonData = (Dictionary<string, object>)jsonObject;
            }
            catch (System.Exception e) 
            { 
                Debug.Log("Deserialization failed :-( " + e.Message);
            }    
			
			//JSon decoding step has failed
			if(jsonData == null)
				return false;
			
			//parse URL information
			m_baseUrl = (jsonData.ContainsKey(URL_DICT_KEY) ?(string)jsonData[URL_DICT_KEY] : null);
			if(m_baseUrl == null || m_baseUrl.Length == 0)
				m_baseUrl = URL_DEFAULT_VALUE;
			
			//parse ClientID information
			m_clientID = (jsonData.ContainsKey(CLIENT_ID_DICT_KEY) ?(string)jsonData[CLIENT_ID_DICT_KEY] : null);
			if(m_clientID == null || m_clientID.Length == 0)
				m_clientID = CLIENT_ID_DEFAULT_VALUE;
			
			
			return true;	//everything went better than expected
             */ 
		}
		
		private TreggSingleton()
		{
			//set the path
			JSON_FILE_PATH = Path.Combine(Application.dataPath, Path.Combine("Resources", "TreggData.json"));
			//Debug.Log("Now the path is " + JSON_FILE_PATH);
			
			//deserialize from file
			if(!DeserializeFromJsonFile(JSON_FILE_PATH))
			{
				//if deserialization has failed init everything with default value and create the file
				m_baseUrl = URL_DEFAULT_VALUE;
				m_clientID = CLIENT_ID_DEFAULT_VALUE;
				SerializeToJsonFile(JSON_FILE_PATH);
			}
		}
		#endregion
				
		#region Tregg attributes accessors
		
		public string URL
		{
			get
			{
				return m_baseUrl;
			}
			
			set
			{
				m_baseUrl = value;
			}
		}
		
		public string ClientID
		{
			get
			{
				return m_clientID;
			}
			
			set
			{
				m_clientID = value;
			}
		}
		
		public void Synchronize()
		{
			SerializeToJsonFile(JSON_FILE_PATH);
		}
		
		#endregion
		
		#region Tregg global methods

        public static void Init(string url, string clientId, Action initCallback)
        {
            Instance.ClientID = clientId;
            Instance.URL = url;
            Init(initCallback);
        }
        
        public static void Init(Action initCallback)
		{
            //init bus object
            Tregg.Bus.Init(() =>
                {
                    TreggSingleton sing = Instance;
                    Tregg.Bus.CallMethod("TreggConfigBus", "init", new string[] { sing.m_baseUrl, sing.m_clientID }, (Dictionary<string, object> results) =>
                        {
                            //results are always empty
                            if (initCallback != null)
                            {
                                initCallback();
                            }
                        });
                });
        }
		
		#endregion
	}
}