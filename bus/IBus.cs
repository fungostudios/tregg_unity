using System;

namespace Tregg
{
	public interface IBus
	{
		void Init();
		void CallMethod(string className, string methodName, string[] methodParams, int cid);
		void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid);
		void Callback(string message);
	}
}

