#if UNITY_IPHONE
using UnityEngine;
using System.Collections;

namespace Tregg
{
    public class IOSBusLoader : Bus.CompiledBusLoader
    {
        protected override IBus bus
        {
            get
            {
                return BusComponentFactory.GetComponent<IOSBus>();
            }
        }
    }
}
#endif