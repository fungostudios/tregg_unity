#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

namespace Tregg
{
    public class EditorBusLoader : Bus.CompiledBusLoader
    {
        protected override IBus bus
        {
            get
            {
                return BusComponentFactory.GetComponent<EditorBus>();
            }
        }
    }
}
#endif