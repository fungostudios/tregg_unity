#if UNITY_WP8
using UnityEngine;
using System.Collections;

namespace Tregg
{
    public class WP8BusLoader : Bus.CompiledBusLoader
    {
        protected override IBus bus
        {
            get
            {
                return BusComponentFactory.GetComponent<WP8Bus>();
            }
        }
    }
}
#endif