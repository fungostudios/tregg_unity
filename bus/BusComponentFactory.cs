using System;
using UnityEngine;

namespace Tregg {
  public class BusComponentFactory
  {
		public const string gameObjectName = "com.fungostudios.bus";
		private static GameObject busGameObject;
		private static GameObject BusGameObject
		{
			get
			{
				if (BusComponentFactory.busGameObject == null)
				{
					BusComponentFactory.busGameObject = new GameObject(gameObjectName);
					GameObject.DontDestroyOnLoad(BusComponentFactory.busGameObject);
				}
				return BusComponentFactory.busGameObject;
			}
		}
		
		public static T GetComponent<T>() where T : MonoBehaviour
		{
			GameObject gameObject = BusComponentFactory.BusGameObject;
			T t = gameObject.GetComponent<T>();
			if (t == null)
			{
				t = gameObject.AddComponent<T>();
			}
			return t;
		}
	}
}