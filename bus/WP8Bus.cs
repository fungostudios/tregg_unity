#if UNITY_WP8
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tregg
{
    sealed class WP8Bus : MonoBehaviour, IBus
    {
        private Assembly[] asms;
        private Dictionary<string, Type> types = new Dictionary<string, Type>(); 

        public void Init() 
        {
            string[] dlls = {/*"Tregg"*/};
            List <Assembly> loadedDlls = new List<Assembly>();
#if BUS_EDITOR_OVERRIDE
            //this force the bus to search routines and classes in unity assembly first. 
            //i.e.: if you define class A method B both in Tregg.dll and as a Unity script the unity version will be used
            loadedDlls.Add(Assembly.GetExecutingAssembly());
#endif
            foreach (string dll in dlls) 
            {
                loadedDlls.Add(Assembly.Load(dll));
            }


            asms = loadedDlls.ToArray();
        }


        private Type GetTypeFor(string className) {
            Type klass;
            if (types.TryGetValue(className, out klass))
                return klass;

            foreach (Assembly asm in asms) { 
                Type t = asm.GetType(className);
                if (t != null) {
                    klass = t;
                    break;
                }
            }

            if (klass == null) {
                throw new Exception("Class " + className + " Not Found.");
            }

            types[className] = klass;
            return klass;
        }

        private string CapitalizeMethodName(string methodName) {
            char[] methodChars = methodName.ToCharArray();
            methodChars[0] = char.ToUpper(methodChars[0]);
            return new string(methodChars);
        }

        public void CallMethod(string className, string methodName, string[] methodParams, int cid) {
            Type klass = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);
            Dictionary<string, object> result = m.Invoke(klass, methodParams) as Dictionary<string, object>;
            if (result == null) //this will happen if m returns void, which is legal
            {
                result = new Dictionary<string, object>();
            }
            result.Add(Bus.CallbackIdKey, cid.ToString());
            Bus.Callback(result);
        }

        public void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid) {
            Type klass = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen + 1];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            types[mLen] = typeof(Action<Dictionary<string, object>>);

            object[] mParams = new object[mLen + 1];
            methodParams.CopyTo(mParams, 0);
            mParams[mLen] = Bus.callbacks[cid];
            Bus.callbacks.Remove(cid);

            // types variable removed, because getMethod doesn't find the method if types are provided
            MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);
            m.Invoke(klass, mParams);
        }

        public void Callback(string message) {
            throw new Exception("Don't call me again!");
        }
    }
}
#endif