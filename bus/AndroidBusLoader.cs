#if UNITY_ANDROID
using UnityEngine;
using System.Collections;

namespace Tregg
{
    public class AndroidBusLoader : Bus.CompiledBusLoader
    {
        protected override IBus bus
        {
            get
            {
                return BusComponentFactory.GetComponent<AndroidBus>();
            }
        }
    }
}
#endif