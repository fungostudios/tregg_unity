#if UNITY_EDITOR
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tregg
{
    sealed class EditorBus : MonoBehaviour, IBus
    {
        private Assembly[] asms;
        private Dictionary<string, Type> types = new Dictionary<string, Type>(); 

        public void Init() 
        {
			string[] dlls = {/*"Tregg"*/};
			List <Assembly> loadedDlls = new List<Assembly>();
			loadedDlls.Add(Assembly.GetExecutingAssembly());
			foreach (string dll in dlls) {
				loadedDlls.Add(Assembly.Load(dll));
			}

			asms = loadedDlls.ToArray();
		}
		
		private Type GetTypeFor(string className) 
        {
			Type klass;
            if (types.TryGetValue(className, out klass))
                return klass;

            foreach (Assembly asm in asms) { 
                Type t = asm.GetType(className);
                if (t != null) {
                    klass = t;
                    break;
                }
            }

            if (klass == null) 
			{
                throw new Exception("Class " + className + " Not Found.");
            }

            types[className] = klass;
            return klass;
        }

        private string CapitalizeMethodName(string methodName) 
        {
            char[] methodChars = methodName.ToCharArray();
            methodChars[0] = char.ToUpper(methodChars[0]);
            return new string(methodChars);
        }

        public void CallMethod(string className, string methodName, string[] methodParams, int cid) 
        {
            Debug.Log("CallMethod(" + className + ", " + methodName + ", " + methodParams + ", " + cid + ")"); 
            Type klass = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);
            Dictionary<string, object> result = m.Invoke(klass, methodParams) as Dictionary<string, object>;
            result.Add(Bus.CallbackIdKey, cid.ToString());
            Bus.Callback(result);
        }

        public void CallAsyncMethod(string className, string methodName, string[] methodParams, int cid) 
        {
            Debug.Log("CallAsyncMethod(" + className + ", " + methodName + ", " + methodParams + ", " + cid + ")");
            Type klass = this.GetTypeFor("Tregg." + className);

            int mLen = methodParams.Length;
            Type stringType = typeof(string);
            Type[] types = new Type[mLen + 1];
            for (int i = 0; i < mLen; i++) {
                types[i] = stringType;
            }

            types[mLen] = typeof(Action<Dictionary<string, object>>);

            object[] mParams = new object[mLen + 1];
            methodParams.CopyTo(mParams, 0);
            mParams[mLen] = Bus.callbacks[cid];
	    Bus.callbacks.Remove(cid); // GC (Garbage Collected) ??

            // types variable removed, because getMethod doesn't find the method if types are provided
            MethodInfo m = klass.GetMethod(CapitalizeMethodName(methodName), types);
            m.Invoke(klass, mParams);
        }

        public void Callback(string message) 
        {
            // Never called
        }
    }
}
#endif
