using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Tregg {
	public class Bus : ScriptableObject {
		public static Dictionary<int, Action<Dictionary<string, object>>> callbacks = new Dictionary<int, Action<Dictionary<string, object>>>();
		private static int callbackCounter;

		private static IBus busImpl;
		public const string CallbackIdKey = "callbackId";
		
		static IBus BusImpl
	    {
	        get
	        {
	            if (busImpl == null)
	            {
	                throw new NullReferenceException("Bus object is not yet loaded.  Did you call Bus.Init()?");
	            }
	            return busImpl;
	        }
	    }

        private static Action s_initCallback = null;
		
		public static void Init(Action initCallback) 
        {
            s_initCallback = initCallback;
#if UNITY_EDITOR
			Debug.Log("Creating the Editor version of the Bus object.");
			BusComponentFactory.GetComponent<EditorBusLoader>();
#elif UNITY_ANDROID
			Debug.Log("Creating the Android version of the Bus object.");
	        BusComponentFactory.GetComponent<AndroidBusLoader>();
#elif UNITY_IOS
			Debug.Log("Creating the Android version of the Bus object.");
	        BusComponentFactory.GetComponent<IOSBusLoader>();
#elif UNITY_WP8
			Debug.Log("Creating the WP8 version of the Bus object.");
	        BusComponentFactory.GetComponent<WP8BusLoader>();
#elif UNITY_METRO
			Debug.Log("Creating the Metro version of the Bus object.");
	        BusComponentFactory.GetComponent<MetroBusLoader>();
#else
			throw new NotImplementedException("Platform not supported");
#endif
        }
	
		private static void OnDllLoaded()
	    {
	        Debug.Log("Finished loading Bus dll.");
	        BusImpl.Init();
            if (s_initCallback != null)
            {
                s_initCallback();
                s_initCallback = null;
            }
	    }
		
		public static int NextCallbackId() {
			callbackCounter++;
			return callbackCounter;
		}

		public static void CallMethod(string className, string methodName, string[] methodParams, Action<Dictionary<string, object>> callback) {
			int cid = NextCallbackId();
			callbacks.Add(cid, callback);
			
			BusImpl.CallMethod(className, methodName, methodParams, cid);
		}
		
		public static void CallAsyncMethod(string className, string methodName, string[] methodParams, Action<Dictionary<string, object>> callback) {
			int cid = NextCallbackId();
			callbacks.Add(cid, callback);
			
			BusImpl.CallAsyncMethod(className, methodName, methodParams, cid);
		}
		
		public static void Callback(string message) {
			Debug.Log("MESSAGE (IN A BOTTLE)");
			Debug.Log(message);
			var rawResult = MiniJSON.Json.Deserialize(message) as Dictionary<string, object>;
			Callback(rawResult);
		}
		
		public static void Callback(Dictionary<string, object> rawData) {
			int cid = Convert.ToInt32(rawData[CallbackIdKey]);
			rawData.Remove(CallbackIdKey);
			callbacks[cid](rawData);
			callbacks.Remove(cid); // GC (Garbage Collected) ??
		}
		
		public abstract class CompiledBusLoader : MonoBehaviour
	    {
	        protected abstract IBus bus { get; }
	 
	        void Start()
	        {
	            Bus.busImpl = bus;
	            Bus.OnDllLoaded();
	            Destroy(this);
	        }
	    }
	}
}
