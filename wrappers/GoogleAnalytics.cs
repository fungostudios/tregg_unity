﻿#if !UNITY_EDITOR && UNIVERSAL_ANALYTICS_PLUGIN
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if (UNITY_WP8 || UNITY_METRO)
//use Analytics wrapper dll
using UniversalAnalyticsWrapper;
#elif UNITY_IOS
//use interop service assemblies for DllImport
using System.Runtime.InteropServices;
#endif


namespace Analytics
{
    public class GoogleAnalytics
    {
        #region Singleton accessors

        private static GoogleAnalytics s_instance;
        public static GoogleAnalytics Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new GoogleAnalytics();
                }

                return s_instance;
            }
        }
        
        #endregion

        private string m_trackingAppId;
        private string m_trackingAppName;

#if UNITY_ANDROID
        public const string INIT_METHOD_NAME_ANDROID = "init";
        public const string CLASS_NAME_ANDROID = "com.fungostudios.googleanalyticswrapper.GoogleAnalyticsWrapper";

        private AndroidJavaClass m_androidTracker;
#elif UNITY_IOS

        [DllImport("__Internal")]
        private static extern void GAI_Init(string _trackingAppId, string _trackingAppName);

        [DllImport("__Internal")]
        private static extern void GAI_SendEvent(string _category, string _action, string _label, int _value);
#endif


        public void Init(string trackingAppId, string trackingAppName)
        {
            m_trackingAppId = trackingAppId;
            m_trackingAppName = trackingAppName;
#if UNITY_METRO || UNITY_WP8
            //TODO use dispatcher to init the plugin
            //init is hardcoded into VStudio project
            Dictionary<string, object> config = new Dictionary<string, object>();

            config.Add(UniversalAnalytics.APP_NAME_KEY, m_trackingAppName);
            config.Add(UniversalAnalytics.TRACKING_ID_KEY, m_trackingAppId);

            Debug.Log("Configuring plugin...");

            //force Universal Analytics wrapper initialization
            UniversalAnalytics.Instance(config);
#elif UNITY_IOS
            GAI_Init(trackingAppId, trackingAppName);
#elif UNITY_ANDROID
            //instantiate android tracker object...
            m_androidTracker = new AndroidJavaClass(CLASS_NAME_ANDROID);
            //...and init the tracker itself
            #if DEBUG
            m_androidTracker.CallStatic(INIT_METHOD_NAME_ANDROID);
            #endif
#endif
        }

#if UNITY_ANDROID
        public const string SEND_VIEW_METHOD_ANDROID = "trackAppView";
#endif

        public void SendView(string screenName)
        {
#if UNITY_WP8 || UNITY_METRO
            UniversalAnalytics.Instance().SendView(screenName);
#elif UNITY_ANDROID
            m_androidTracker.CallStatic(SEND_VIEW_METHOD_ANDROID, new object[] { m_trackingAppId, screenName });
#endif
        }

#if UNITY_ANDROID
        public const string SEND_EVENT_METHOD_ANDROID = "trackEvent";
#endif
        public void SendEvent(string category, string action, string label = "", int value = 0)
        {
            Debug.Log("Send event " + category + "." + action + "." + label + " = " + value);
            try
            {
#if UNITY_WP8 || UNITY_METRO
                UniversalAnalytics.Instance().SendEvent(category, action, label, value);
#elif UNITY_IOS
                GAI_SendEvent(category, action, label, value);
#elif UNITY_ANDROID
            	m_androidTracker.CallStatic(SEND_EVENT_METHOD_ANDROID, new object[] { m_trackingAppId, category, action, label, value });
#endif
            }
            catch (System.Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception : " + e.ToString());
            }
        }

        public void SendException(string excMessage, bool isFatal = false)
        {
#if UNITY_WP8 || UNITY_METRO
            UniversalAnalytics.Instance().SendException(excMessage, isFatal);
#endif
        }

        public void SendTransaction(string transactionId, double cost, string itemSku, string itemName = null, long qtyPurchased = 1)
        {
            if(string.IsNullOrEmpty(itemName))
            {
                itemName = itemSku;
            }

#if UNITY_WP8 || UNITY_METRO
            UniversalAnalytics.Instance().SendTransaction(transactionId, cost, itemSku, itemName, qtyPurchased);
#endif
        }

        public void SendSocialEvent(string socialNetwork, string action, string target = "")
        {
#if UNITY_WP8 || UNITY_METRO
            UniversalAnalytics.Instance().SendSocialEvent(socialNetwork, action, target);
#endif
        }

        public void SendTiming(string category, string variable,  double seconds, string label = "")
        {
#if UNITY_WP8 || UNITY_METRO
            UniversalAnalytics.Instance().SendTiming(category, variable, label, seconds);
#endif
        }
    }
}
#endif
