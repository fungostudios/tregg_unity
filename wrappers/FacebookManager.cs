﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Tregg;

#if BUS_EDITOR_OVERRIDE
using TreggUser = Tregg.TreggUser;
#else
using TreggUser = Tregg.Unity.TreggUser;
#endif

#if UNITY_WP8
using Prime31;
using Prime31.WinPhoneSocialNetworking;
#elif UNITY_METRO
using Prime31;
using Prime31.MetroSocial;
#endif

public class FacebookManager
{
    private string m_fbAppId;
    private string m_fbAppSecret;

    private string m_fbLoginRedirectUrl;
    public string FbLoginRedirectUrl
    {
        get
        {
            return m_fbLoginRedirectUrl;
        }

        set
        {
            m_fbLoginRedirectUrl = value;
        }
    }

	//private members
    private Dictionary<string, object> m_meResultDict;
	
	//singleton class
	private static FacebookManager s_instance = null;
	//singleton accessor
	public static FacebookManager Instance
	{
		get
		{            
			if(s_instance == null)
				s_instance = new FacebookManager();
			
			return s_instance;
		}
	}

    //initializer method

    public void Init(string appId, string appSecret, string redirectUrl)
    {
        if (string.IsNullOrEmpty(appId) || string.IsNullOrEmpty(appSecret) || string.IsNullOrEmpty(redirectUrl))
        {
            throw new ArgumentNullException("Can't initialize facebook : null values are not allowed.");
        }

        m_fbAppId = appId;
#if UNITY_WP8 || UNITY_METRO
        FacebookAccess.applicationId = m_fbAppId;
#endif
        m_fbAppSecret = appSecret;
        m_fbLoginRedirectUrl = redirectUrl;
    }
	
	//private constructor
	private FacebookManager() 
	{
		Debug.Log("Initing facebook app");
	
#if UNITY_WP8 && !UNITY_EDITOR
		//Initing Prime31 SocialNetworking plugin
		FacebookAccess.applicationId = m_fbAppId;
		if( !GameObject.Find( "WinPhoneFacebook" ) )
		{
			var go = new GameObject( "WinPhoneFacebook" );
			var mb = go.AddComponent<MonoBehaviourGUI>();
			Prime31.Facebook.instance.prepareForMetroUse( go, mb );
		}
#elif UNITY_METRO
        FacebookAccess.applicationId = FB_APP_ID;
        if (!GameObject.Find("MetroFacebook"))
        {
            var go = new GameObject("MetroFacebook");
            var mb = go.AddComponent<MonoBehaviourGUI>();
            Prime31.Facebook.instance.prepareForMetroUse(go, mb);
        }
#elif UNITY_ANDROID || UNITY_IOS

        Debug.Log("Initing Android/iOS FB plugin.");

        FB.Init(OnInitComplete, OnHideUnity);
#endif

        m_meResultDict = null;
	}

#if UNITY_ANDROID || UNITY_IOS 
    #region Android init callbacks

    private void OnInitComplete()
    {
#if UNITY_IOS
        FB.PublishInstall((FBResult result) => { Debug.Log(result.Text); });
        FB.AppEvents.LogEvent(Facebook.FBAppEventName.ActivatedApp);
#elif UNITY_ANDROID

        //direct call on FB Unity SDK isn't working
        //[COMMANDO PATTERN] engaged

        //fetch current application context
        Debug.Log("fetch current application context");
        AndroidJavaObject unityContext = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getApplicationContext");

        //call "activateApp" static method via reflection
        Debug.Log("call activateApp static method via reflection");
        new AndroidJavaClass("com.facebook.AppEventsLogger").CallStatic("activateApp", new object[] { unityContext });
        
        //[COMMANDO PATTERN] disengaged

#endif
        Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
    }

    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log("Is game showing? " + isGameShown);
    }

    #endregion
#endif


    private void doTreggLogin(string facebookAccessToken, Action postLoginOp = null, Action<bool> postFBLoginOp = null)
	{
		if(!IsSessionValid)
		{
			Debug.LogError("Can't login properly on FB");
		}
		else 
		{
			TreggUser.SignInWithFacebook(AccessToken, (TreggUser user, TreggException exc) => 
			{
                bool loggedIn = true;
                FetchUserInfo();

				TreggUser currentUser = user;
				if (exc != null && exc.Message == "You are already authenticated") 
				{
					#if !BUS_EDITOR_OVERRIDE
					Tregg.Unity.TreggUser.CurrentUser (
						(TreggUser _user, Tregg.TreggException _exc) => {
						if (_exc != null) {
							exc = _exc;
						}else {
							currentUser = _user;
						}
					});
					#endif
				}

				if(exc != null && exc.Message != "You are already authenticated") 
				{
					Debug.LogError("Failed to log in with error : " + exc.Message);
                    loggedIn = false;
				}
				else
				{
					string currId = (currentUser != null ? currentUser.Uid.ToString() : ">>> NOT FOUND! <<<");						
					Debug.Log("Obtained player ID " + currId);
					if(postLoginOp != null)
					{
						postLoginOp();
					}
				}

                if (postFBLoginOp != null)
                {
                    postFBLoginOp(loggedIn);
                }
			});			
		}
	}

    private const string HTTP_CLIENT_CLASS = "TreggHttpClientBus";
    private const string HTTP_CLIENT_METHOD = "createGetRequest";
    private const string HTTP_CLIENT_URL = "https://graph.facebook.com/me";
    private const string HTTP_CLIENT_ACCESS_TOKEN_PARAM_KEY = "access_token";
    private const string HTTP_CLIENT_JSON_BODY = "JSON_BODY";
    private void FetchUserInfo()
    {
        Dictionary<string, string> callParams = new Dictionary<string,string>();
        callParams.Add(HTTP_CLIENT_ACCESS_TOKEN_PARAM_KEY, AccessToken);
        string paramJson = MiniJSON.Json.Serialize(callParams);

        Tregg.Bus.CallAsyncMethod(HTTP_CLIENT_CLASS, HTTP_CLIENT_METHOD, new string[] {HTTP_CLIENT_URL, paramJson, "{}"}, (Dictionary<string, object> responseData) =>
        {
            if (responseData.ContainsKey(HTTP_CLIENT_JSON_BODY) && responseData[HTTP_CLIENT_JSON_BODY] != null && !string.IsNullOrEmpty(responseData[HTTP_CLIENT_JSON_BODY].ToString()))
            {
                m_meResultDict = MiniJSON.Json.Deserialize(responseData[HTTP_CLIENT_JSON_BODY].ToString()) as Dictionary<string, object>;
            }
            else
            {
                m_meResultDict = null;
            }
        });
    }
	
	private void OnSuccessfulLogin(bool isLoggedIn, bool performTreggLogin = true, Action postTreggLoginOp = null, Action<bool> postFBLoginOp = null)
	{
		if(performTreggLogin && isLoggedIn)
		{
			doTreggLogin(AccessToken, postTreggLoginOp, postFBLoginOp);
		}
        else if (postFBLoginOp != null)
        {
            postFBLoginOp(isLoggedIn);
        }
	}
	
	/**
	 * Perform the login with facebook. 
	 * If a valid session exists login is not performed, unless the forceNewLogin parameter is set to true.
	 */ 
	public void Login(bool performTreggLogin = true, Action postTreggLoginOp = null, Action<bool> postFBLoginOp = null, bool forceNewLogin = false)
	{
		// Facebook login will not work unless the redirectUrl used is exactly as it appears in the Facebook developer
		// website under the Website with Facebook Login Site URL field
		Debug.Log("Attempting FB login...");
        string[] permissions = new string[] { "publish_actions", "user_birthday", "email", "publish_stream" };

        bool logged = false;

        try
        {

#if UNITY_WP8
            if (!FacebookAccess.isSessionValid() || forceNewLogin)
            {
                FacebookAccess.login(FbLoginRedirectUrl, m_fbAppSecret, permissions, (string accessToken, System.Exception error) =>
                {
                    try
                    {
                        if (error != null)
                        {
                            Debug.LogError("Error logging in: " + error);
                        }
                        else
                        {
                            Debug.Log("Login successful. Access token: " + accessToken);
                            logged = true;
                        }

                    }
                    catch (SystemException exc)
                    {
                        Debug.LogError("FB login failed : " + exc.Message);
                        logged = false;
                    }
                    finally
                    {
                        OnSuccessfulLogin(logged, performTreggLogin, postTreggLoginOp, postFBLoginOp);
                    }
                });
            }
            else
            {
                OnSuccessfulLogin(true, performTreggLogin, postTreggLoginOp, postFBLoginOp);
            }
#elif UNITY_METRO

            FacebookAccess.login(permissions, (string accessToken) =>
                {
                    if (!string.IsNullOrEmpty(this.AccessToken))
                    {
                        Debug.Log("Login successful. Access token: " + this.AccessToken);
                        logged = true;
                    }
                    else
                    {
                        Debug.LogError("Unable to login to FB.");
                    }

                    OnSuccessfulLogin(logged, performTreggLogin, postTreggLoginOp, postFBLoginOp);
                });

#elif UNITY_ANDROID || UNITY_IOS

        FB.Login(String.Join(",", permissions), (FBResult result) =>
            {
                if (result.Error != null)
                {
                    Debug.Log( "Error Response:\n" + result.Error);
                }
                else if (!FB.IsLoggedIn)
                {
                    Debug.Log("Login cancelled by Player");
                }
                else
                {
                    Debug.Log("Login with FB completed successfully");
                    logged = true;
                }

                OnSuccessfulLogin(logged, performTreggLogin, postTreggLoginOp, postFBLoginOp);

            });
#endif
        }
        catch (System.ArgumentException argExc)
        {
            Debug.Log("Login canceled by user.\n" + argExc.Message);
            if (postFBLoginOp != null)
            {
                postFBLoginOp(logged);
            }
        }
        catch (System.Exception exc)
        {
            Debug.LogError("Failed to login with facebook : " + exc.Message);
            if (postFBLoginOp != null)
            {
                postFBLoginOp(logged);
            }
        }
    }
	
	/**
	 * Logs out from facebook, destroying any existing session
	 */
	public void Logout()
    {
#if UNITY_WP8 || UNITY_METRO
        FacebookAccess.logout();	
#elif UNITY_ANDROID || UNITY_IOS
        FB.Logout();
#endif

    //    Tregg.TreggUserBus.SignOut();

	}
	
	/**
	 * Returns true if the current session is a valid session
	 */
	
	public bool IsSessionValid
	{
		get
		{
#if UNITY_WP8 || UNITY_METRO
            return FacebookAccess.isSessionValid();
#elif UNITY_ANDROID || UNITY_IOS
            return FB.IsLoggedIn;
#else
            return false;
			#endif
		}
	}
	
	/**
	 * Returns the access token of the current facebook session.
	 * If session doesn't exists or is invalid returns an empty string.
	 */ 
	public string AccessToken
	{
		get
		{
			if(IsSessionValid)
            {
#if UNITY_WP8 || UNITY_METRO

                return FacebookAccess.accessToken;
				
#elif UNITY_ANDROID || UNITY_IOS

                return FB.AccessToken;
#else

                return "";
				
#endif
			}
			else
				return "";
		}
	}
	
	public void Invite(string inviteTitle, string inviteMessage)
	{
		//if there's not a current valid session (unlikely) prompt user for login again
		if(!IsSessionValid)
			Login();
		
		// prepare the required dialog parameters (message is required)
		if(IsSessionValid)
        {
#if UNITY_WP8

            var parameters = new Dictionary<string,object>();
			parameters.Add( "message", inviteMessage );
			parameters.Add( "title", inviteTitle );
		
			FacebookAccess.showDialog( "apprequests", parameters, dialogResultUrl =>
			{
		        if(dialogResultUrl != null)
				    Debug.Log( "dialog completed with url: " + dialogResultUrl );
		        else
		            Debug.LogError("Failed to request.");
			});
#elif UNITY_METRO
            throw new System.NotImplementedException(); //TODO not exposed in Prime31 plugin: find workaround
#endif
		}
		else
			Debug.LogError("Session is invalid, invitation sending skipped.");		
	}

    public const string FEED_DIALOG_APP_ID_KEY = "app_id";
    public const string FEED_DIALOG_LINK_URL_KEY = "link";
    public const string FEED_DIALOG_PICTURE_URL_KEY = "picture";
    public const string FEED_DIALOG_LINK_NAME_KEY = "name";
    public const string FEED_DIALOG_LINK_CAPTION_KEY = "caption";
    public const string FEED_DIALOG_LINK_DESCRIPTION_KEY = "description";
	
	public void PostOnWall(string linkUrl, string picUrl, string linkName, string linkCaption, string linkDescription, Action<string> callback = null)
	{
		
		Debug.Log("Attempting to post on FB wall...");
		
        if(callback == null)
        {
            callback = (string postUrl) => 
                {
                    try
                    {
                        if (postUrl == null)
                        {
                            Debug.LogError("Failed to post on facebook");
                        }
                    }
                    catch (System.Exception exc)
                    {
                        Debug.LogError("Failed to post on FB : " + exc.Message);
                    }
                };
        }

#if UNITY_WP8

        Dictionary<string, object> feedParams = new Dictionary<string,object>();
        feedParams.Add(FEED_DIALOG_APP_ID_KEY, m_fbAppId);
        feedParams.Add(FEED_DIALOG_LINK_URL_KEY, linkUrl);
        feedParams.Add(FEED_DIALOG_PICTURE_URL_KEY, picUrl);
        feedParams.Add(FEED_DIALOG_LINK_NAME_KEY, linkName);
        feedParams.Add(FEED_DIALOG_LINK_CAPTION_KEY, linkCaption);
        feedParams.Add(FEED_DIALOG_LINK_DESCRIPTION_KEY, linkDescription);

        FacebookAccess.showDialog("feed", feedParams, callback);

#elif UNITY_METRO

        Dictionary<string, object> feedParams = new Dictionary<string,object>();
        feedParams.Add(FEED_DIALOG_APP_ID_KEY, FB_APP_ID);
        feedParams.Add(FEED_DIALOG_LINK_URL_KEY, linkUrl);
        feedParams.Add(FEED_DIALOG_PICTURE_URL_KEY, picUrl);
        feedParams.Add(FEED_DIALOG_LINK_NAME_KEY, linkName);
        feedParams.Add(FEED_DIALOG_LINK_CAPTION_KEY, linkCaption);
        feedParams.Add(FEED_DIALOG_LINK_DESCRIPTION_KEY, linkDescription);

        //post directly to FB without asking for message text
        Prime31.MetroSocial.FacebookAccess.graphRequestPost("/me/feed", feedParams, (object ret) =>
            {
                callback(ret != null ? ret.ToString() : "NULL"); 
            });

        //TODO implement user feedback mechanism
#elif UNITY_ANDROID || UNITY_IOS

		FB.Feed(
			link: linkUrl,
			linkName: linkName,
			linkCaption: linkCaption,
			linkDescription: linkDescription,
			picture: picUrl,
			callback: (FBResult response) =>
			{
				Debug.Log("PostOnWall Facebook result: "+response.Text);
			}
		);

#endif
    }

    #region Facebook user data accessors

    private string obtainDataFromMeDict(string key)
    {
        if (m_meResultDict.ContainsKey(key) && m_meResultDict[key] != null)
        {
            return m_meResultDict[key].ToString();
        }
        else
        {
            return string.Empty;
        }
    }

    public const string FB_USER_FIRST_NAME_KEY = "first_name";

    public string UserFirstName
    {
        get
        {
            return obtainDataFromMeDict(FB_USER_FIRST_NAME_KEY);
        }

    }

    public const string FB_USER_LAST_NAME_KEY = "last_name";
    public string UserLastName
    {
        get
        {
            return obtainDataFromMeDict(FB_USER_LAST_NAME_KEY);
        }

    }

    public const string FB_USER_BIRTHDAY_KEY = "birthday";
    public DateTime UserBirthday
    {
        get
        {
            DateTime ret;
            if (DateTime.TryParse(obtainDataFromMeDict(FB_USER_BIRTHDAY_KEY), out ret))
            {
                return ret;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

    }

    public const string FB_USER_GENDER_KEY = "gender";
    public const string FB_MALE_GENDER_STRING = "male";
    public const string FB_FEMALE_GENDER_STRING = "female";
    public enum GenderEnum
    {
        Unknown = 0,
        Male,
        Female,
        COUNT
    }
    public GenderEnum UserIsMale
    {
        get
        {
            switch (obtainDataFromMeDict(FB_USER_GENDER_KEY))
            {
                case FB_MALE_GENDER_STRING:
                    return GenderEnum.Male;
                case FB_FEMALE_GENDER_STRING:
                    return GenderEnum.Female;
                default:
                    return GenderEnum.Unknown;
            }
        }

    }
    #endregion
}