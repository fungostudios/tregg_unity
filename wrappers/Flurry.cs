﻿#if !UNITY_EDITOR && FLURRY_PLUGIN

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_WP8 
using FlurryWP8SDK;
using FlurryWP8SDK.Models;
#endif

public class Flurry
{
    private static bool s_sessionStarted = false;
    public static bool SessionStarted
    {
        get
        {
            return s_sessionStarted;
        }

        protected set
        {
            s_sessionStarted = value;
        }
    }

    public enum SupportedPlatformEnum
    {
        WP8 = 0,
        Android,
        iOS,
        COUNT
    }

    public static void StartSession(Dictionary<SupportedPlatformEnum, string> appKeys)
    {

#if UNITY_WP8
        SupportedPlatformEnum currPlatform = SupportedPlatformEnum.WP8;
#elif UNITY_ANDROID
        SupportedPlatformEnum currPlatform = SupportedPlatformEnum.Android;
#elif UNITY_IOS
        SupportedPlatformEnum currPlatform = SupportedPlatformEnum.iOS;
#else
        SupportedPlatformEnum currPlatform = SupportedPlatformEnum.COUNT;
#endif

        if(!appKeys.ContainsKey(currPlatform))
        {
            throw new System.Exception("Unsupported platform for flurry plugin!!!");
        }

        string appKey = appKeys[currPlatform];

        if (SessionStarted)
        {
            throw new System.Exception("Already existing session. End the old one before starting another.");
        }

        bool noErrorsHappened = true;

        Api.StartSession(appKey);

        SessionStarted = noErrorsHappened;
    }

    private static string s_userID;
    public static string UserID
    {
        get
        {
            return s_userID;
        }
    }

    public static void SetUserID(string newUserId)
    {
        if (newUserId != s_userID)
        {
            Api.SetUserId(s_userID);		  
        }

        s_userID = newUserId;
    }

    public static void LogEvent(string eventName, bool isTimed, Dictionary<string, object> eventParameters)
    {

        List<Parameter> eventParamList = new List<Parameter>();

        foreach (KeyValuePair<string, object> pair in eventParameters)
        {
            eventParamList.Add(new Parameter(pair.Key, pair.Value != null ? pair.Value.ToString() : null));
        }

        Api.LogEvent(eventName, eventParamList, isTimed);

    }

    public static void EndTimedEvent(string eventName, Dictionary<string, object> eventParameters)
    {

        List<Parameter> eventParamList = new List<Parameter>();

        foreach (KeyValuePair<string, object> pair in eventParameters)
        {
            eventParamList.Add(new Parameter(pair.Key, pair.Value != null ? pair.Value.ToString() : null));
        }

        Api.EndTimedEvent(eventName, eventParamList);

    }
}
#endif