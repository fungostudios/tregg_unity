using System.Collections.Generic;

namespace Tregg
{
    namespace Unity {
        public class TreggUserNativeResultHandler : TreggUser.ITreggUserResultHandler {
            public void Process(Dictionary<string, object> rawData, TreggUser.TreggUserDelegate callback) {
				Tregg.TreggException error = null;

                if (rawData.ContainsKey(Constants.ERROR_KEY) && rawData[Constants.ERROR_KEY] != null)
                {
                    error = (TreggException)rawData[Constants.ERROR_KEY];
				}

				if (error != null)
                {
                    callback(null, error);
                }
                else
                {
					Tregg.TreggUser u = (Tregg.TreggUser)rawData[Constants.USER_KEY];
                    callback(u != null ? new TreggUser(u) : null, null);
                }
            }
        }
    }
}
