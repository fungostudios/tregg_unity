using UnityEngine;
using System;
using System.Collections.Generic;

namespace Tregg
{
    namespace Unity {
        public class TreggUser {
            public delegate void TreggUserDelegate(TreggUser user, Tregg.TreggException e);

            private string userName;
            private string password;
            private string facebookUid;
            private int userId;
            private static ITreggUserResultHandler resultHandler;

            private static void ResultProcessor(Dictionary<string, object> rawData, TreggUserDelegate callback) { ResultHandler.Process(rawData, callback); }

            public string UserName
            {
                set { userName = value; }
                get { return userName; }
            }

            public string Password
            {
                set { password = value; }
                get { return password; }
            }

            public string FacebookUid
            {
                set { facebookUid = value; }
                get { return facebookUid; }
            }

            public int Uid
            {
                private set { userId = value; }
                get { return userId; }
            }

            public static ITreggUserResultHandler ResultHandler
            {
                get {
                    if (resultHandler != null) { return resultHandler; }

#if UNITY_EDITOR || UNITY_WP8 || UNITY_METRO
                    resultHandler = new TreggUserNativeResultHandler();
#elif UNITY_ANDROID || UNITY_IOS
                    resultHandler = new TreggUserJsonResultHandler();
#else
                    throw new NotImplementedException("Platform not supported");
#endif

                   return resultHandler;
                }
            }

            public TreggUser(string name, string pwd)
            {
                this.UserName = name;
                this.Password = pwd;
            }

            public TreggUser(Tregg.TreggUser user) {
                this.UserName = user.UserName;
                this.Password = user.Password;
                this.FacebookUid = user.FacebookUid;
                this.Uid = user.Uid;
            }

            public TreggUser(Dictionary<string, object> jsonUser) {
				this.UserName = jsonUser["username"].ToString();
				this.FacebookUid = jsonUser["facebook_uid"].ToString();
                this.Uid = Convert.ToInt32(jsonUser["id"]);
            }

            public static void SignUp(string username, string pwd, TreggUserDelegate callback)
            {
                TreggUser user = new TreggUser(username, pwd);
                user.SignUp(callback);
            }

            public void SignUp(TreggUserDelegate callback) {
                string[] methodParams = { UserName, Password };
                TreggUser.BusAsyncCall("signUp", methodParams, callback);
            }

            public static void SignIn(string username, string password, TreggUserDelegate callback) {
                string[] methodParams = { username, password };
                TreggUser.BusAsyncCall("signIn", methodParams, callback);
            }

            public static void SignInWithFacebook(string accessToken, TreggUserDelegate callback) {
                string[] methodParams = { accessToken};
                TreggUser.BusAsyncCall("signInWithFacebook", methodParams, callback);
            }

            public void LinkFacebook(string accessToken, TreggUserDelegate callback) {
                string[] methodParams = { accessToken};
                TreggUser.BusAsyncCall("signInWithFacebook", methodParams, callback);
            }

            public static void CurrentUser(TreggUserDelegate callback) {
                string [] methodParams = { };
                TreggUser.BusCall("currentUser", methodParams, callback);
            }

            private static void BusCall(string methodName, string[] methodParams, TreggUserDelegate callback) {
                Tregg.Bus.CallMethod("TreggUserBus", methodName, methodParams, (Dictionary<string, object> rawData) =>
                        {
                            ResultProcessor(rawData, callback);
                        }
                );
            }

            private static void BusAsyncCall(string methodName, string[] methodParams, TreggUserDelegate callback) {
                Tregg.Bus.CallAsyncMethod("TreggUserBus", methodName, methodParams, (Dictionary<string, object> rawData) =>
                        {
                            ResultProcessor(rawData, callback);
                        }
                );
            }

            public interface ITreggUserResultHandler {
                void Process(Dictionary<string, object> rawData, TreggUserDelegate callback);
            }
        }
    }
}
