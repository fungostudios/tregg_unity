using UnityEngine;
using System.Collections.Generic;

namespace Tregg
{
    namespace Unity 
    {
        public class TreggUserJsonResultHandler : TreggUser.ITreggUserResultHandler 
        {
            public void Process(Dictionary<string, object> rawData, TreggUser.TreggUserDelegate callback) 
            {
            
                string error = null;
				if (rawData.ContainsKey(Constants.EXCEPTION_KEY) && rawData[Constants.EXCEPTION_KEY] != null)
                {
					error = rawData[Constants.EXCEPTION_KEY].ToString();
                }

                if (!string.IsNullOrEmpty(error))
                {
                    callback(null, new Tregg.TreggException(error));
                }
                else
                {
					callback(new TreggUser((Dictionary<string, object>)rawData[Constants.USER_KEY]), null);
                }
            }
        }
    }
}