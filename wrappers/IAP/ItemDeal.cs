﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public class ItemDeal : Deal
    {
        #region Static members

        public const string TREGG_DEFAULT_PURCH_URL = "/virtual/purchases";

        private static string s_treggPurchUrl = TREGG_DEFAULT_PURCH_URL;

        public static string TreggPurchURL 
        {
            get
            {
                return s_treggPurchUrl;
            }

            set
            {
                s_treggPurchUrl = value;
            }
        }

        #endregion

        #region Attributes

        private string m_itemDealId;

        public string ItemDealID
        {
            get
            {
                return m_itemDealId;
            }

            internal set
            {
                m_itemDealId = value;
            }
        }

        private List<string> m_inputItemsIdsList;

        public IList<string> InputItemsIDList
        {
            get
            {
                return m_inputItemsIdsList;
            }

            internal set
            {
                m_inputItemsIdsList = new List<string>(value);
            }
        }

        #endregion

        #region Ctors, Dtors, Initializers and Builders

        public ItemDeal(string id, string itemDealId, string storeProductId, string name, string description, string imageUrl, string tags, DateTime startDate, DateTime finishDate, bool onSale, IList<string> outItemsIds, IList<string> inItemsIdsList)
            : base(id, name, description, imageUrl, tags, startDate, finishDate, onSale, outItemsIds)
        {
            ItemDealID = itemDealId;
            InputItemsIDList = inItemsIdsList;
        }

        public const string ITEM_DEAL_ID_KEY = "id";
        public const string IN_ITEMS_IDS_LIST_KEY = "in_item_ids";
        public const string BASE_DEAL_ID = "base_deal_id";

        protected override void updateFieldFromPair(KeyValuePair<string, object> pair)
        {
            switch (pair.Key)
            {
                case ITEM_DEAL_ID_KEY:
                    ItemDealID = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case IN_ITEMS_IDS_LIST_KEY:
                    InputItemsIDList = listFromObject(pair.Value);
                    break;
                default:
                    base.updateFieldFromPair(pair);
                    break;
            }
        }

        protected override void populateDictionary(IDictionary<string, object> dict)
        {
            base.populateDictionary(dict);
            dict[ITEM_DEAL_ID_KEY] = ItemDealID;
            dict.Add(BASE_DEAL_ID, ID);
            dict.Add(IN_ITEMS_IDS_LIST_KEY, InputItemsIDList);
        }

        #endregion

        #region Deal purchase methods

        public const string DEAL_ID_KEY = "item_deal_id";
        public const string DEST_INVENTORY_ID_KEY = "inventory_id";
        public const string SRC_INVENTORY_ID_KEY = "source_inventory_id";

        public void PurchaseDeal(string inventoryId, Action<PurchaseSuccessLevel> callback)
        {
            PurchaseDeal(inventoryId, inventoryId, callback);
        }

        public override void PurchaseDeal(string destInventoryId, string srcInventoryId, Action<PurchaseSuccessLevel> callback)
        {
            Dictionary<string, string> reqParams = new Dictionary<string, string>();
            reqParams.Add(DEAL_ID_KEY, m_itemDealId);
            reqParams.Add(DEST_INVENTORY_ID_KEY, destInventoryId);
            reqParams.Add(SRC_INVENTORY_ID_KEY, srcInventoryId);

            Tregg.Unity.TreggApi.CreatePostRequest(s_treggPurchUrl, reqParams, (Dictionary<string, object> results) =>
                {
                    if (!results.ContainsKey(Constants.ERROR_KEY))
                    {
                        //TODO: handle the correct purchase return data. IE: piggyback updated informations into local data models
                        callback(PurchaseSuccessLevel.Success);
                    }
                    else
                    {
                        callback(PurchaseSuccessLevel.TreggError);
                    }
                }, true);
        }

        #endregion
    }
}

#endif
