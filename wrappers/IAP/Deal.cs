﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
namespace Tregg.InAppPurchase
{
    public abstract class Deal
    {
        #region Attributes

        private string m_id;

        /// <summary>
        /// The key used to map ID field in serialization/deserialization processes
        /// </summary>
        public const string ID_KEY = "id";

        /// <summary>
        /// The deal ID
        /// </summary>
        public string ID
        {
            get
            {
                return m_id;
            }

            internal set
            {
                m_id = value;
            }
        }

        private string m_name;

        /// <summary>
        /// The key used to map name field in serialization/deserialization processes
        /// </summary>
        public const string NAME_KEY = "name";

        /// <summary>
        /// The deal ID
        /// </summary>
        public string Name
        {
            get
            {
                return m_name;
            }

            internal set
            {
                m_name = value;
            }
        }

        private string m_description;

        /// <summary>
        /// The key used to map description field in serialization/deserialization processes
        /// </summary>
        public const string DESCRIPTION_KEY = "description";

        /// <summary>
        /// The deal ID
        /// </summary>
        public string Description
        {
            get
            {
                return m_description;
            }

            internal set
            {
                m_description = value;
            }
        }

        private string m_imageUrl;

        /// <summary>
        /// The key used to map image url field in serialization/deserialization processes
        /// </summary>
        public const string IMAGE_URL_KEY = "image_url";

        /// <summary>
        /// The deal ID
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return m_imageUrl;
            }

            internal set
            {
                m_imageUrl = value;
            }
        }

        private string m_tags;

        /// <summary>
        /// The key used to map tags field in serialization/deserialization processes
        /// </summary>
        public const string TAGS_KEY = "tags";

        /// <summary>
        /// The deal ID
        /// </summary>
        public string Tags
        {
            get
            {
                return m_tags;
            }

            internal set
            {
                m_tags = value;
            }
        }

        private DateTime m_startDate;

        /// <summary>
        /// The key used to map start date field in serialization/deserialization processes
        /// </summary>
        public const string START_DATE_KEY = "start_date";

        /// <summary>
        /// The deal starting date
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                return m_startDate;
            }

            internal set
            {
                m_startDate = value;
            }
        }

        /// <summary>
        /// The key used to map finish date field in serialization/deserialization processes
        /// </summary>
        public const string FINISH_DATE_KEY = "finish_date";

        /// <summary>
        /// The deal finish date
        /// </summary>
        public DateTime FinishDate
        {
            get
            {
                return m_startDate;
            }

            internal set
            {
                m_startDate = value;
            }
        }

        private bool m_onSale;

        /// <summary>
        /// The key used to map On Sale field in serialization/deserialization processes
        /// </summary>
        public const string ON_SALE_KEY = "on_sale";

        /// <summary>
        /// The deal On Sale flag
        /// </summary>
        public bool OnSale
        {
            get
            {
                return m_onSale;
            }

            internal set
            {
                m_onSale = value;
            }
        }

        private List<string> m_outItemIdList;

        /// <summary>
        /// The key used to map Out Item Id List field in serialization/deserialization processes
        /// </summary>
        public const string OUT_ITEM_IDS_LIST_KEY = "out_item_ids";

        /// <summary>
        /// The deal ID
        /// </summary>
        public IList<string> OutItemIdList
        {
            get
            {
                return new List<string>(m_outItemIdList);
            }
        }

        #endregion

        #region Ctors, Dtors, Initializers and Builders

        /// <summary>
        /// Explicit constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="imageUrl"></param>
        /// <param name="tags"></param>
        /// <param name="startDate"></param>
        /// <param name="finishDate"></param>
        /// <param name="onSale"></param>
        /// <param name="outItemsIds"></param>
        protected Deal(string id, string name, string description, string imageUrl, string tags, DateTime startDate, DateTime finishDate, bool onSale, IEnumerable<string> outItemsIds)
        {
            ID = id;
            Name = name;
            Description = description;
            ImageUrl = imageUrl;
            Tags = tags;
            StartDate = startDate;
            FinishDate = finishDate;
            m_outItemIdList = outItemsIds != null ? new List<string>(outItemsIds) : null;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        protected Deal()
            : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.MinValue, DateTime.MinValue, false, null)
        { }

        ///Serializers / Deserializers
        #region DateTime related parsing methods

        public static readonly DateTime UNIX_EPOCH_START = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        protected static DateTime dateTimeFromEpoch(long epochTime)
        {
            return dateTimeFromEpoch(epochTime, UNIX_EPOCH_START);
        }

        protected static DateTime dateTimeFromEpoch(long epochTime, DateTime epochStart)
        {
            return epochStart.AddSeconds(epochTime);
        }

        protected static DateTime dateTimeFromObject(object input)
        {
            DateTime ret = DateTime.MinValue;

            if (input != null)
            {
                Type inType = input.GetType();

                if (typeof(long).IsAssignableFrom(inType))   //the input it's a long: parse as epoch date
                {
                    ret = dateTimeFromEpoch((long)input);
                }
                else if (!string.IsNullOrEmpty(input.ToString()))    //the input is a string: try some parse techniques
                {
                    if (!DateTime.TryParse(input.ToString(), out ret))   //try to parse a date from the text
                    {
                        long x;
                        //if it's failed the string may be a long! Retry 
                        if (long.TryParse(input.ToString(), out x))
                        {
                            //successfully parsed
                            ret = dateTimeFromEpoch(x);
                        }
                    }
                }

                //if everything has failed try Convert
                if (ret == DateTime.MinValue)
                {
                    ret = Convert.ToDateTime(input);
                }
            }

            return ret;

        }

        #endregion

        #region List related parsing methods
        
        protected static List<string> listFromObject(object input)
        {
            List<string> ret = null;
            Type retType = typeof(List<string>);
            if (input != null)
            {
                if (typeof(IEnumerable<string>).IsAssignableFrom(input.GetType()))
                {
                    ret = new List<string>(input as IEnumerable<string>);
                }
                else if(!string.IsNullOrEmpty(input.ToString()))
                {
                    object jsonObj = MiniJSON.Json.Deserialize(input.ToString());
                    Type jsonType = jsonObj.GetType();
                    if (retType.IsAssignableFrom(jsonType))
                    {
                        ret = jsonObj as List<string>;
                    }
                    else if (typeof(IDictionary<string, object>).IsAssignableFrom(jsonType))
                    {
                        IDictionary<string, object> jsonDict = jsonObj as IDictionary<string, object>;
                        IEnumerator<object> jsonEnumerator = jsonDict.Values.GetEnumerator();
                        if (jsonDict.Count < 2 && jsonEnumerator.MoveNext())    //process if only 1 key
                        {
                            ret = listFromObject(jsonEnumerator.Current);   //recursion to parse the contained object
                        }
                    }

                }
            }

            return ret;
        }

        #endregion

        /// <summary>
        /// Updates a single object field
        /// </summary>
        /// <param name="pair">A key-value pair which contains both the key to identify the field to update and the new value to assign to it</param>
        /// <remarks>This method is used by deserialization methods and should be extended in child classes if new fields to be deserialized are inserted</remarks>
        protected virtual void updateFieldFromPair(KeyValuePair<string, object> pair)
        {
            switch (pair.Key)
            {
                case DESCRIPTION_KEY:
                    Description = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case FINISH_DATE_KEY:
                    FinishDate = dateTimeFromObject(pair.Value);
                    break;
                case ID_KEY:
                    ID = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case IMAGE_URL_KEY:
                    ImageUrl = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case NAME_KEY:
                    Name = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case ON_SALE_KEY:
                    OnSale = Convert.ToBoolean(pair.Value);
                    break;
                case OUT_ITEM_IDS_LIST_KEY:
                    m_outItemIdList = listFromObject(pair.Value);
                    break;
                case START_DATE_KEY:
                    StartDate = dateTimeFromObject(pair.Value);
                    break;
                case TAGS_KEY:
                    Tags = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
            }
        }

        /// <summary>
        /// Updates all the object fields values from a dictionary
        /// </summary>
        /// <param name="dict">The dictionary with the new values</param>
        /// <remarks>The keys for the different values are the "_KEY"-suffixed consts defined in the class. Any unrecognized key will be ignored and any missing key will leave the related field unaltered</remarks>
        public virtual void UpdateFromDict(IDictionary<string, object> dict)
        {
            foreach (KeyValuePair<string, object> pair in dict)
            {
                updateFieldFromPair(pair);
            }
        }

        /// <summary>
        /// Updates all the object fields values from a JSON-represented dictionary
        /// </summary>
        /// <param name="json">The JSON-represented dictionary with the new fields values</param>
        /// <remarks>This will deserialize the JSON to a dictionary and then will call the <see cref="UpdateFromDict(IDictionary<string, object> dict)"/> method.</remarks>
        public virtual void UpdateFromJSON(string json)
        {
            UpdateFromDict(MiniJSON.Json.Deserialize(json) as IDictionary<string, object>);
        }

        /// <summary>
        /// This inserts all the fields values into the given IDictionary object 
        /// </summary>
        /// <param name="dict">The IDictionary object to be populated</param>
        /// <remarks>This method is used by serialization methods. This should be extended in child classes if new fields to be serialized are inserted</remarks>
        protected virtual void populateDictionary(IDictionary<string, object> dict)
        {
            dict.Add(DESCRIPTION_KEY, Description);
            dict.Add(FINISH_DATE_KEY, FinishDate);
            dict.Add(ID_KEY, ID);
            dict.Add(IMAGE_URL_KEY, ImageUrl);
            dict.Add(NAME_KEY, Name);
            dict.Add(ON_SALE_KEY, OnSale);
            dict.Add(OUT_ITEM_IDS_LIST_KEY, OutItemIdList);
            dict.Add(START_DATE_KEY, StartDate);
            dict.Add(TAGS_KEY, Tags);
        }

        /// <summary>
        /// Returns a key-value-based representation of the object
        /// </summary>
        /// <returns>An IDictionary which contains the fields of the object, using the constants defineds in the class as keys.</returns>
        /// <remarks>This dictionary can be correctly processed by the <see cref="UpdateFromDict"/> and <see cref="FromDict"/> methods.</remarks>
        public virtual IDictionary<string, object> ToDict()
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();

            populateDictionary(ret);

            return ret;
        }


        /// <summary>
        /// Returns a JSON representation of the object
        /// </summary>
        /// <returns>A string containing the JSON representation of the object</returns>
        /// <remarks>This will invoke the <see cref="ToDict"/> method and serialize its result as JSON</remarks>
        public virtual string ToJSON()
        {
            return MiniJSON.Json.Serialize(ToDict());
        }

        //No builders are defined for this class, because it's abstract

        #endregion

        #region Deal fullfilment method

        public enum PurchaseSuccessLevel
        {
            StoreError = 0,
            TreggError,
            Success,
            COUNT
        }

        /// <summary>
        /// This starts the deal purchasing process
        /// </summary>
        /// <param name="callback">This callback is called on purchase completion. Callback parameter is set to true iff the purchase have been successfull.</param>
        public abstract void PurchaseDeal(string destInventoryId, string srcInventoryId, Action<PurchaseSuccessLevel> callback);

        #endregion
    }
}

#endif
