﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public class Item
    {
        #region Attributes and accessors
        private string m_id;
        public string ID
        {
            get
            {
                return m_id;
            }

            internal set
            {
                m_id = value;
            }
        }

        private string m_name;
        public string Name
        {
            get
            {
                return m_name;
            }

            internal set
            {
                m_name = value;
            }
        }

        private string m_description;
        public string Description
        {
            get
            {
                return m_description;
            }

            internal set
            {
                m_description = value;
            }
        }

        private string m_imageUrl;
        public string ImageURL
        {
            get
            {
                return m_imageUrl;
            }

            internal set
            {
                m_imageUrl = value;
            }
        }

        private string m_tags;
        public string Tags
        {
            get
            {
                return m_tags;
            }

            internal set
            {
                m_tags = value;
            }
        }

        private string m_formattedPrice;

        public string FormattedPrice
        {
            get
            {
                return m_formattedPrice;
            }

            internal set
            {
                m_formattedPrice = value;
            }
        }

        #endregion

        #region Ctors, Dtors and initializers

        public Item(string id, string name, string description, string imageUrl, string tags)
        {
            ID = id;
            Description = description;
            Name = name;
            ImageURL = imageUrl;
            Tags = tags;
        }

        public Item()
            : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)
        {
        }

        #endregion

        #region Serialization / Deserialization

        public const string ID_KEY = "id";
        public const string DESCRIPTION_KEY = "description";
        public const string NAME_KEY = "name";
        public const string IMAGE_URL_KEY = "image_url";
        public const string TAGS_KEY = "tags";

        public const string FORMATTED_PRICE_KEY = "Price";

        protected void UpdateFieldFromPair(KeyValuePair<string, object> pair)
        {
            switch (pair.Key)
            {
                case ID_KEY:
                    ID = pair.Value.ToString();
                    break;
                case DESCRIPTION_KEY:
                    Description = pair.Value.ToString();
                    break;
                case NAME_KEY:
                    Name = pair.Value.ToString();
                    break;
                case IMAGE_URL_KEY:
                    ImageURL = pair.Value.ToString();
                    break;
                case TAGS_KEY:
                    Tags = pair.Value.ToString();
                    break;
                case FORMATTED_PRICE_KEY:
                    FormattedPrice = pair.Value.ToString();
                    break;
            }
        }

        public void DeserializeFromDict(IDictionary<string, object> dict)
        {
            foreach (KeyValuePair<string, object> pair in dict)
            {
                UpdateFieldFromPair(pair);
            }
        }

        public void DeserializeFromJSON(string json)
        {
            DeserializeFromDict((IDictionary<string, object>)MiniJSON.Json.Deserialize(json));
        }

        protected void buildDict(IDictionary<string, object> dict)
        {
            dict.Add(ID_KEY, ID);
            dict.Add(DESCRIPTION_KEY, Description);
            dict.Add(NAME_KEY, Name);
            dict.Add(IMAGE_URL_KEY, ImageURL);
            dict.Add(TAGS_KEY, Tags);
        }

        public IDictionary<string, object> SerializeToDict()
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();

            buildDict(ret);

            return ret;
        }

        public string SerializeToJSON()
        {
            return MiniJSON.Json.Serialize(SerializeToDict());
        }

        #endregion

    }
}
#endif