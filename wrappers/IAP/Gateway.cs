﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public class Gateway
    {
        #region Platform dependent gateway name

#if UNITY_WP8
        public const string GATEWAY_PROVIDER_NAME = "wp8";
#elif UNITY_METRO
        public const string GATEWAY_PROVIDER_NAME = "w8";
#elif UNITY_ANDROID
        public const string GATEWAY_PROVIDER_NAME = "android";
#elif UNITY_IOS
        public const string GATEWAY_PROVIDER_NAME = "apple";
#else
        /// <summary>
        /// This is the gateway name expected for the given platform
        /// </summary>
        public const string GATEWAY_PROVIDER_NAME = string.Empty;
#endif
        #endregion

        #region Attributes

        private string m_id;

        /// <summary>
        /// The key used for ID field during dict-based serialization/deserialization procedures
        /// </summary>
        public const string ID_KEY = "id";
        /// <summary>
        /// The gateway ID
        /// </summary>
        /// <remarks>The deals will reference the gateway by this field</remarks>
        public string ID
        {
            get
            {
                return m_id;
            }

            internal set
            {
                m_id = value;
            }
        }

        private string m_name;

        /// <summary>
        /// The key used for Name field during dict-based serialization/deserialization procedures
        /// </summary>
        public const string NAME_KEY = "name";
        /// <summary>
        /// The gateway name
        /// </summary>
        /// <remarks>This field is the only information you can use to recognize the correct gateway for the used platform</remarks>
        public string Name
        {
            get
            {
                return m_name;
            }

            internal set
            {
                m_name = value;
            }
        }

        private string m_providerUrl;

        /// <summary>
        /// The key used for provider URL field during dict-based serialization/deserialization procedures
        /// </summary>
        public const string PROVIDER_URL_KEY = "provider_url";
        /// <summary>
        /// This is the provider URL
        /// </summary>
        public string ProviderUrl
        {
            get
            {
                return m_providerUrl;
            }

            internal set
            {
                m_providerUrl = value;
            }
        }

        private string m_publicKey;

        /// <summary>
        /// The key used for public key field during dict-based serialization/deserialization procedures
        /// </summary>
        public const string PUBLIC_KEY_KEY = "public_key";
        /// <summary>
        /// The public store key
        /// </summary>
        public string PublicKey
        {
            get
            {
                return m_publicKey;
            }

            internal set
            {
                m_publicKey = value;
            }
        }

        #endregion

        #region Ctors, DTors, Initializers and Builders

        /// <summary>
        /// Explicit construcor
        /// </summary>
        /// <param name="id">The gateway ID</param>
        /// <param name="name">The gateway name</param>
        /// <param name="providerUrl">The gateway provider url</param>
        /// <param name="publicKey">The gateway public key</param>
        public Gateway(string id, string name, string providerUrl, string publicKey)
        {
            m_id = id;
            m_name = name;
            m_providerUrl = providerUrl;
            m_publicKey = publicKey;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <remarks>This will call the explicit constructor with empty strings as parameters values</remarks>
        public Gateway() : this(string.Empty, string.Empty, string.Empty, string.Empty)
        {}

        protected virtual void updateFieldFromPair(KeyValuePair<string, object> pair)
        {
            switch (pair.Key)
            {
                case ID_KEY:
                    ID = pair.Value.ToString();
                    break;
                case NAME_KEY:
                    Name = pair.Value.ToString();
                    break;
                case PROVIDER_URL_KEY:
                    ProviderUrl = pair.Value.ToString();
                    break;
                case PUBLIC_KEY_KEY:
                    PublicKey = pair.Value.ToString();
                    break;
            }
        }

        /// <summary>
        /// Updates all the gateway fields values from a dictionary
        /// </summary>
        /// <param name="dict">The dictionary with the new values</param>
        /// <remarks>The keys for the different values are the "_KEY"-suffixed consts defined in the class. Any unrecognized key will be ignored and any missing key will leave the related field unaltered</remarks>
        public void UpdateFromDict(IDictionary<string, object> dict)
        {
            foreach (KeyValuePair<string, object> pair in dict)
            {
                updateFieldFromPair(pair);
            }
        }

        /// <summary>
        /// Updates all the gateway fields values from a JSON-represented dictionary
        /// </summary>
        /// <param name="json">The JSON-represented dictionary with the new fields values</param>
        /// <remarks>This will deserialize the JSON to a dictionary and then will call the <see cref="UpdateFromDict(IDictionary<string, object> dict)"/> method.</remarks>
        public void UpdateFromJSON(string json)
        {
            UpdateFromDict(MiniJSON.Json.Deserialize(json) as IDictionary<string, object>);
        }

        protected virtual void buildDictFromFields(Dictionary<string, object> dict)
        {
            dict.Add(ID_KEY, ID);
            dict.Add(NAME_KEY, Name);
            dict.Add(PUBLIC_KEY_KEY, PublicKey);
            dict.Add(ProviderUrl, PROVIDER_URL_KEY);

        }

        /// <summary>
        /// Returns a key-value-based representation of the object
        /// </summary>
        /// <returns>An IDictionary which contains the fields of the object, using the constants defineds in the class as keys.</returns>
        /// <remarks>This dictionary can be correctly processed by the <see cref="UpdateFromDict"/> and <see cref="FromDict"/> methods.</remarks>
        public IDictionary<string, object> ToDict()
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();

            buildDictFromFields(ret);

            return ret;
        }


        /// <summary>
        /// Returns a JSON representation of the object
        /// </summary>
        /// <returns>A string containing the JSON representation of the object</returns>
        /// <remarks>This will invoke the <see cref="ToDict"/> method and serialize its result as JSON</remarks>
        public string ToJSON()
        {
            return MiniJSON.Json.Serialize(ToDict());
        }

        /// <summary>
        /// Builds a Gateway object from its key-value-based representation
        /// </summary>
        /// <param name="dict">The IDictionary containing the field values</param>
        /// <returns>The Gateway object initialized from the dictioary values</returns>
        /// <remarks>This calls the <see cref="UpdateFromDict"/> method to initialize the object fields.</remarks>
        public static Gateway FromDict(IDictionary<string, object> dict)
        {
            Gateway ret = new Gateway();
            ret.UpdateFromDict(dict);
            return ret;
        }

        /// <summary>
        /// Builds a Gateway object from its JSON representation
        /// </summary>
        /// <param name="json">The JSON containing the field values</param>
        /// <returns>The Gateway object initialized from the JSON values</returns>
        /// <remarks>This calls the <see cref="UpdateFromJSON"/> method to initialize the object fields.</remarks>
        public static Gateway FromJSON(string json)
        {
            Gateway ret = new Gateway();
            ret.UpdateFromJSON(json);
            return ret;
        }
        #endregion
    }
}

#endif
