﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public class GatewayDeal : Deal
    {

#if DEBUG && UNITY_IOS
        private const bool SANDBOX_MODE = true;
#else
        private const bool SANDBOX_MODE = false;
#endif

        #region Attributes

        private string m_gatewayDealId;

        public string GatewayDealID
        {
            get
            {
                return m_gatewayDealId;
            }

            internal set
            {
                m_gatewayDealId = value;
            }
        }

        private string m_storeProductId;

        public string StoreProductId
        {
            get
            {
                return m_storeProductId;
            }

            internal set
            {
                m_storeProductId = value;
            }
        }

        private string m_gatewayId;

        public string GatewayID
        {
            get
            {
                return m_gatewayId;
            }

            internal set
            {
                m_gatewayId = value;
            }
        }

        #endregion

        #region Ctors, Dtors, Initializers and Builders

        public GatewayDeal(string id, string gatewayDealId, string storeProductId, string name, string description, string imageUrl, string tags, DateTime startDate, DateTime finishDate, bool onSale, IList<string> outItemsIds, string gatewayId)
            : base(id, name, description, imageUrl, tags, startDate, finishDate, onSale, outItemsIds)
        {
            GatewayDealID = gatewayDealId;
            StoreProductId = storeProductId;
            GatewayID = gatewayId;
        }

        public GatewayDeal()
            : base()
        {
        }

        public const string GATEWAY_DEAL_ID_KEY = "id";
        public const string PRODUCT_ID_KEY = "product_id";
        public const string GATEWAY_ID_KEY = "gateway_id";
        public const string BASE_DEAL_ID = "base_deal_id";
        public const string BASE_DEAL_GATEWAY_DEAL_IDS = "gateway_deal_ids";

        protected override void updateFieldFromPair(KeyValuePair<string, object> pair)
        {
            switch (pair.Key)
            {
                case GATEWAY_DEAL_ID_KEY:
                    if (string.IsNullOrEmpty(GatewayDealID))
                    {
                        GatewayDealID = pair.Value == null ? string.Empty : pair.Value.ToString();
                        break;
                    }
                    else    //this is the base deal id
                    {
                        goto default;
                    }
                case GATEWAY_ID_KEY:
                    GatewayID = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                case PRODUCT_ID_KEY:
                    StoreProductId = pair.Value == null ? string.Empty : pair.Value.ToString();
                    break;
                default:
                    base.updateFieldFromPair(pair);
                    break;
            }
        }

        protected override void populateDictionary(IDictionary<string, object> dict)
        {
            base.populateDictionary(dict);
            dict[GATEWAY_DEAL_ID_KEY] = GatewayDealID;
            dict.Add(BASE_DEAL_ID, ID);
            dict.Add(GATEWAY_ID_KEY, GatewayID);
            dict.Add(PRODUCT_ID_KEY, StoreProductId);
        }

        #endregion

        #region Static members

        public const string TREGG_DEFAULT_PURCH_URL = "/{0}/purchases";

        private static string s_treggPurchUrl = TREGG_DEFAULT_PURCH_URL;

        public static string TreggPurchURL
        {
            get
            {
                return s_treggPurchUrl;
            }

            set
            {
                s_treggPurchUrl = value;
            }
        }

        #endregion

        #region Deal purchase methods

        public const string GATEWAY_DEAL_ID_PARAM = "gateway_deal_id";
        public const string RECEIPT_PARAM = "receipt";
        public const string TRANSACTION_ID_PARAM = "transaction_id";
        public const string PRODUCT_ID_PARAM = "product_id";
        public const string INVENTORY_ID_PARAM = "inventory_id";
        public const string SANDBOX_PARAM = "sandbox";

        public override void PurchaseDeal(string inventoryId, string gatewayName, Action<PurchaseSuccessLevel> callback)
        {
            PurchaseManager.Instance.RequestProductPurchase(m_storeProductId, (bool success, string transactionId, string receipt) =>
                {
                    if (success)
                    {
                        Dictionary<string, string> reqParams = new Dictionary<string, string>();
                        reqParams.Add(GATEWAY_DEAL_ID_PARAM, m_gatewayDealId);
                        reqParams.Add(RECEIPT_PARAM, receipt);
                        reqParams.Add(TRANSACTION_ID_PARAM, transactionId);
                        reqParams.Add(PRODUCT_ID_PARAM, m_storeProductId);
                        reqParams.Add(INVENTORY_ID_PARAM, inventoryId);
                        reqParams.Add(SANDBOX_PARAM, SANDBOX_MODE.ToString().ToLower());

                        Tregg.Unity.TreggApi.CreatePostRequest(string.Format(s_treggPurchUrl, gatewayName), reqParams, (Dictionary<string, object> results) =>
                            {
                                if (!results.ContainsKey(Constants.ERROR_KEY))
                                {
                                    //TODO: handle the correct purchase return data. IE: piggyback updated informations into local data models
                                    callback(PurchaseSuccessLevel.Success);
                                    PurchaseManager.Instance.ReportProductFulfillment(m_storeProductId);    //after callback call product fulfillment routine
                                }
                                else
                                {
                                    //something has gone horribly wrong :-(
                                    callback(PurchaseSuccessLevel.TreggError);
#if DEBUG_IAP
                                    PurchaseManager.Instance.ReportProductFulfillment(m_storeProductId);    //TODO remove, debug only
#endif
                                }
                            }, true);
                    }
                    else
                    {

                        callback(PurchaseSuccessLevel.StoreError);
#if DEBUG_IAP
                                    PurchaseManager.Instance.ReportProductFulfillment(m_storeProductId);    //TODO remove, debug only
#endif
                    }
                });

        }

        #endregion
    }
}

#endif
