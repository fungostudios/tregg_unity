﻿#if UNITY_WP8 && TREGG_IN_APP_PURCHASE
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using TreggNativeStore;

namespace Tregg.InAppPurchase
{
    public class PurchaseManager_WP8 : PurchaseManager
    {

        public PurchaseManager_WP8()
            : base()
        {
        }

        private static Item obtainProduct(InAppProduct inApp)
        {
            Item ret = new Item();

            ret.DeserializeFromDict(inApp.ToDict());

            return ret;
        }

        public override void ObtainListingInformation(IDictionary<string, object> catalogue, Action<bool> completionHandler)
        {
            try
            {
                InAppPurchaseManager.ObtainListingInformation((IDictionary<string, InAppProduct> list) =>
                    {
                        try
                        {
                            foreach (string itemId in new List<string>(catalogue.Keys))
                            {
                                if (list.ContainsKey(itemId))
                                {
                                    catalogue[itemId] = obtainProduct(list[itemId]);
                                }
                            }

                            completionHandler(true);
                        }
                        catch (System.Exception exc)
                        {
                            Debug.LogError("Can't obtain listing informations : " + exc.Message);
                            completionHandler(false);
                        }
                    });
            }
            catch (System.Exception exc)
            {
                Debug.LogError("Can't obtain listing informations : " + exc.Message);
                completionHandler(false);
            }
        }

        public override void RequestProductPurchase(string productId, PurchaseHandlingDelegate callback)
        {
            InAppPurchaseManager.RequestProductPurchase(productId, (bool success, string receipt) =>
                {
                    string transactionId = string.Empty;
                    if (success)
                    {
                        try
                        {
                            using (System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(new System.IO.StringReader(receipt)))
                            {
                                xmlReader.ReadToFollowing("ProductReceipt");
                                xmlReader.MoveToAttribute("Id");
                                transactionId = xmlReader.Value;
                            }
                        }
                        catch (Exception exc)
                        {
                            Debug.LogError("Can't purchase product : " + exc.Message);
                        }
                    }

                    callback(success, transactionId, receipt);
                });
        }
        
        public override void GetProductReceipt(string productId, Action<string> receiptHandler)
        {
            InAppPurchaseManager.GetProductReceipt(productId, receiptHandler);
        }

        public override void ReportProductFulfillment(string productId)
        {
            InAppPurchaseManager.ReportProductFulfillment(productId);
        }
    }
}

#endif