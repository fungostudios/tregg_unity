﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public abstract class PurchaseManager
    {
        #region Singleton accessors

        private static PurchaseManager s_instance;
        public static PurchaseManager Instance
        {
            get
            {
                if (s_instance == null)
                {
#if UNITY_WP8
                    s_instance = new PurchaseManager_WP8();
#else
                    throw new System.PlatformNotSupportedException("No in-app purchase manager defined for this platform");
#endif
                }

                return s_instance;
            }
        }

        #endregion

        #region CTors

        /// <summary>
        /// Default constructor
        /// </summary>
        protected PurchaseManager()
        {
        }
		 
	    #endregion

        #region Attributes

        protected IDictionary<string, Item> m_listingInfos;

        /// <summary>
        /// The listing informations of all In-App items
        /// </summary>
        /// <remarks>If you want to access only one In-App items use the <see cref="ObtainItem(string itemId)"/> method.</remarks>
        public IDictionary<string, Item> ListingInformations
        {
            get
            {
                return new Dictionary<string, Item>(m_listingInfos);
            }
        }

        #endregion

        /// <summary>
        /// Obtain an In-App item's informations
        /// </summary>
        /// <param name="itemId">The in-app item's ID</param>
        /// <returns>An Item object with the desired informations or NULL if the given ID doesn't exists</returns>
        /// <remarks>Before calling this methods is mandatory to succesfully call <see cref="ObtainListingIngormation"/> at least once.</remarks>
        public virtual Item ObtainItem(string itemId)
        {
            if (m_listingInfos.ContainsKey(itemId))
            {
                return m_listingInfos[itemId];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Download the listing informations of in-app items
        /// </summary>
        /// <param name="catalogue">The IDictionary object to save the downloaded informations into. This must already contains, as keys, the IDs of all the items you want to download the infos.</param>
        /// <param name="completionHandler">This callback will be fired once the data download is complete. The only parameter will be true iff the operation has been successful.</param>
        public abstract void ObtainListingInformation(IDictionary<string, object> catalogue, Action<bool> completionHandler);

        public delegate void PurchaseHandlingDelegate(bool success, string transactionId, string receipt);

        ///// <summary>
        ///// Requests a product purchase for the given item.
        ///// </summary>
        ///// <param name="product">The in-app product you want to buy</param>
        ///// <param name="completionHandler">A callback which will be fired after the purchase. The Item parameter is a reference to the bought item, the bool parameter indicates the success of the purchase and the string parameter contains the XML purchase receipt</param>
        //public virtual void RequestProductPurchase(Item product, Action<Item, bool, string> completionHandler)
        //{
        //    RequestProductPurchase(product.ID, (bool success, string receipt) =>
        //        {
        //            completionHandler(product, success, receipt);
        //        });
        //}

        /// <summary>
        /// Requests a product purchase for the given productId.
        /// </summary>
        /// <param name="productId">The in-app product ID you want to buy</param>
        /// <param name="completionHandler">A callback which will be fired after the purchase. The bool parameter indicates the success of the purchase and the string parameter contains the XML purchase receipt</param>
        public abstract void RequestProductPurchase(string productId, PurchaseHandlingDelegate callback);

        /// <summary>
        /// Gets the product receipt for the given productId.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="receiptHandler"></param>
 
        public abstract void GetProductReceipt(string productId, Action<string> receiptHandler);

        /// <summary>
        /// report product fullfillment
        /// </summary>
        /// <param name="productId"></param>
        public abstract void ReportProductFulfillment(string productId);

    }
}
#endif