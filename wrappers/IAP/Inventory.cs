﻿#if TREGG_IN_APP_PURCHASE
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.InAppPurchase
{
    public class Inventory
    {
        #region Ctors
		
        public Inventory()
        {
            m_inventoryContents = new Dictionary<Item,float>();
        }
 
	    #endregion

        #region Attributes

        private string m_inventoryId;
        public const string INVENTORY_ID_KEY = "INVENTORY_ID_KEY";

        public string ID
        {
            get
            {
                return m_inventoryId;
            }

            internal set
            {
                m_inventoryId = value;
            }
        }

        #endregion

        #region Contents handling

        protected Dictionary<Item, float> m_inventoryContents;
        public IDictionary<Item, float> InventoryContents
        {
            get
            {
                return new Dictionary<Item, float>(m_inventoryContents);
            }
        }

        public float getItemQty(Item item)
        {
            float ret;

            if (!m_inventoryContents.TryGetValue(item, out ret))
            {
                ret = 0;
            }

            return ret;
        }

        #endregion
    }
}

#endif