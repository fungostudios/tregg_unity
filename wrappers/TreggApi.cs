using System;
using System.Collections.Generic;

namespace Tregg 
{
    namespace Unity 
    {
        public class TreggApi 
        {
            private static void BusAsyncCall(string methodName, string[] methodParams,
                Action<Dictionary<string, object>> callback)
            {
                Tregg.Bus.CallAsyncMethod("TreggAPIBus", methodName,
                    methodParams, callback);
            }

            public static void CreatePostRequest(string url,
                Dictionary<string, string> requestParams,
                Action<Dictionary<string, object>> callback, bool isAuthenticated,
                string contentType = "application/x-www-form-urlencoded")
            {
                requestParams.Add("is_authenticated", isAuthenticated.ToString().ToLower());
                string jsonParams = MiniJSON.Json.Serialize(requestParams);
                string[] methodParams = { url, jsonParams, contentType };
                BusAsyncCall("createPostRequest", methodParams, callback);
            }

            public static void CreateGetRequest(string url,
                Dictionary<string, string> requestParams,
                Action<Dictionary<string, object>> callback,
                bool isAuthenticated)
            {
                requestParams.Add("is_authenticated", isAuthenticated.ToString().ToLower());
                string jsonParams = MiniJSON.Json.Serialize(requestParams);
                string[] methodParams = { url, jsonParams };
                BusAsyncCall("createGetRequest", methodParams, callback);
            }

            public static void CreatePutRequest(string url,
                Dictionary<string, string> requestParams,
                Action<Dictionary<string, object>> callback,
                bool isAuthenticated,
                string contentType = "application/x-www-form-urlencoded")
            {
                requestParams.Add("is_authenticated", isAuthenticated.ToString().ToLower());
                string jsonParams = MiniJSON.Json.Serialize(requestParams);
                string[] methodParams = { url, jsonParams, contentType };
                BusAsyncCall("createPutRequest", methodParams, callback);
            }

            public static void CreateDeleteRequest(string url,
                Dictionary<string, string> requestParams,
                Action<Dictionary<string, object>> callback,
                bool isAuthenticated)
            {
                requestParams.Add("is_authenticated", isAuthenticated.ToString().ToLower());
                string jsonParams = MiniJSON.Json.Serialize(requestParams);
                string[] methodParams = { url, jsonParams };
                BusAsyncCall("createDeleteRequest", methodParams, callback);
            }
        }
    }
}
