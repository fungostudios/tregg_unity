namespace Tregg
{
    public class Constants
    {
        public const string USER_KEY = "user";
        public const string RESPONSE_BODY_KEY = "JSON_BODY";
        public const string ERROR_KEY = "error";
        public const string EXCEPTION_KEY = ERROR_KEY;
    }
}
