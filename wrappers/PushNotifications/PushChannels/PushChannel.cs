﻿#if PUSH_NOTIFICATIONS
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Tregg.Unity.PushNotifications
{
    public abstract class PushChannel
    {
        #region members and accessors

        protected string m_channelName;
        public virtual string ChannelName
        {
            get
            {
                return m_channelName;
            }
            protected set
            {
                m_channelName = value;
            }
        }

        protected string m_channelUri;
        public string ChannelURI
        {
            get
            {
                return m_channelUri;
            }

            protected set
            {
                m_channelUri = value;
            }
        }

        #endregion

        #region CTors, DTors, builders and initializers

        internal PushChannel()
        {
            Error = null;
            UriUpdate = null;
            ToastNotificationReceived = null;
            RawNotificationReceived = null;
        }

        ~PushChannel()
        {
            Error = null;
            UriUpdate = null;
            ToastNotificationReceived = null;
            RawNotificationReceived = null;
        }

        #endregion

        #region Push channel callbacks and events

        public delegate void PushEventHandler(IDictionary<string, object> eventData);

        public event PushEventHandler Error;
        public event PushEventHandler UriUpdate;
        public event PushEventHandler ToastNotificationReceived;
        public event PushEventHandler RawNotificationReceived;

        protected virtual void OnPushChannelError(IDictionary<string, object> errorData)
        {
            if (Error != null)
            {
                Error(errorData);
            }
        }

        protected static string m_newChannelUriKey;
        public static string NEW_CHANNEL_URI_KEY
        {
            get
            {
                return m_newChannelUriKey;
            }
        }


        public const string OLD_CHANNEL_URI_KEY = "OLD_CHANNEL_URI_KEY";
        protected virtual void OnUriUpdateCallback(IDictionary<string, object> updateData)
        {
            if (updateData.ContainsKey(NEW_CHANNEL_URI_KEY) && updateData[NEW_CHANNEL_URI_KEY] != null && updateData[NEW_CHANNEL_URI_KEY].ToString() != ChannelURI)
            {
                if (!string.IsNullOrEmpty(ChannelURI))
                {
                    updateData.Add(OLD_CHANNEL_URI_KEY, ChannelURI);
                }

                ChannelURI = updateData[NEW_CHANNEL_URI_KEY].ToString();
            }

            if (UriUpdate != null)
            {
                UriUpdate(updateData);
            }
        }

        protected virtual void OnToastCallback(IDictionary<string, object> toastData)
        {
            if (ToastNotificationReceived != null)
            {
                ToastNotificationReceived(toastData);
            }
        }

        protected virtual void OnRawNotificationCallback(IDictionary<string, object> notifData)
        {
            if (RawNotificationReceived != null)
            {
                RawNotificationReceived(notifData);
            }
        }

        #endregion

        #region Channel management methods

        public abstract bool Open();

        public abstract void Close();

        #endregion
    }
} 
#endif