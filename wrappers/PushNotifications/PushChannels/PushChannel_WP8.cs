﻿#if PUSH_NOTIFICATIONS && UNITY_WP8

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NativeChannel = PushNotificationWrapper.PushChannel;

namespace Tregg.Unity.PushNotifications
{
    public class PushChannel_WP8 : PushChannel
    {
        #region Members

        //native channel defined in dll
        private NativeChannel m_pushChannel;

        #endregion

        #region Ctor

        internal PushChannel_WP8(string channelName, string serviceName) : base()
        {
            m_newChannelUriKey = NativeChannel.PUSH_KEY_NEW_CHANNEL_URI;
            m_pushChannel = new NativeChannel(channelName, serviceName, OnPushChannelError, OnUriUpdateCallback, OnToastCallback, OnRawNotificationCallback);
        }

        #endregion

        #region Channel management

        public override bool Open()
        {
            bool ret = true;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add(NativeChannel.OPEN_BIND_TILE_BOOL, true);
            param.Add(NativeChannel.OPEN_BIND_TOAST_BOOL, true);

            m_pushChannel.Open(param);

            if (param.ContainsKey(NativeChannel.EXCEPTION))
            {
                Debug.LogError("Unable to open push channel : " + param[NativeChannel.EXCEPTION]);
                ret = false;
            }
            else if (m_pushChannel.ChannelURI == null)
            {
                Debug.LogError("Unable to open push channel : MISSING URI. Will wait for a new URI from push notification service");
            }
            else
            {
                ChannelURI = m_pushChannel.ChannelURI.ToString();
                Dictionary<string, object> newUriData = new Dictionary<string, object>();
                newUriData.Add(NEW_CHANNEL_URI_KEY, ChannelURI);
                OnUriUpdateCallback(newUriData);
            }

            return ret;
        }

        public override void Close()
        {
            if (!string.IsNullOrEmpty(ChannelURI))
            {
                m_pushChannel.Close(null);
            }
        }

        #endregion
    }
}

#endif