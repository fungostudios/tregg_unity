﻿#if UNITY_WP8 && (PUSH_NOTIFICATIONS || LIVE_TILES_SUPPORT)
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NativeTile = PushNotificationWrapper.Tile;

namespace Tregg.Unity.PushNotifications
{
    public class Tile
    {
        #region Tile list management

        private static List<Tile> s_activeTilesList;

        public static IList<Tile> ActiveTilesList
        {
            get 
            {
                if(s_activeTilesList == null)
                {
                    buildActiveTileList();
                }

                return new List<Tile>(s_activeTilesList);
            }
        }


        #endregion

        #region Ctors, dtors, builders and initializers

        protected Tile(NativeTile nativeTile)
        {
            m_nativeTile = nativeTile;
            FrontTitle = BackTitle = BackContent = string.Empty;
            Counter = 0;
        }

        protected static void buildActiveTileList()
        {
            s_activeTilesList = new List<Tile>();

            foreach (NativeTile nativeTile in PushNotificationWrapper.Tile.ActiveTileList)
            {
                s_activeTilesList.Add(new Tile(nativeTile));
            }
        }
		 
	    #endregion

        #region Tile attributes

        private NativeTile m_nativeTile;


        private string m_frontTitle;

        public string FrontTitle
        {
            get { return m_frontTitle; }
            internal set { m_frontTitle = value; }
        }


        private int m_counter;

        public int Counter
        {
            get { return m_counter; }
            internal set { m_counter = value; }
        }


        private string m_backTitle;

        public string BackTitle
        {
            get { return m_backTitle; }
            internal set { m_backTitle = value; }
        }


        private string m_backContent;

        public string BackContent
        {
            get { return m_backContent; }
            internal set { m_backContent = value; }
        }

        #endregion

        #region Tile management

        private IDictionary<string, object> tileDataToDict()
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();
            ret.Add(NativeTile.FRONT_TITLE_KEY, FrontTitle);
            ret.Add(NativeTile.FRONT_COUNTER_KEY, Counter);
            ret.Add(NativeTile.BACK_CONTENT_KEY, BackContent);
            ret.Add(NativeTile.BACK_TITLE_KEY, BackTitle);

            return ret;
        }

        public void UpdateTile()
        {
            m_nativeTile.ShowNotification(tileDataToDict());
        }

        public void ResetTile(string frontTitle)
        {
            FrontTitle = frontTitle;
            Counter = 0;
            BackTitle = BackContent = string.Empty;
            UpdateTile();
        }
		 
        /****/
        ///global update methods
        /****/

        public static void ResetAllTiles(string frontTitle)
        {
            foreach (Tile tile in ActiveTilesList)
            {
                tile.ResetTile(frontTitle);
            }
        }

        public static void UpdateAllTiles()
        {
            foreach (Tile tile in ActiveTilesList)
            {
                tile.UpdateTile();
            }
        }

	    #endregion
    }
}
#endif