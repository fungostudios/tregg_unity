using UnityEngine;
using System;
using System.Collections.Generic;

namespace Tregg {
    namespace Unity {
        public class TreggHttpClient {
            public interface ITreggHttpClientResultHandler {
                void Process(Dictionary<string, object> response, Action<Dictionary<string, object>> callback);
            }

            public static void CreatePostRequest(String url, Dictionary<string, string> requestParams,
                    Dictionary<string, string> requestHeaders, Action<Dictionary<string, object>> callback,
                    string contentType = "application/x-www-form-urlencoded") {
                string jsonParams  = MiniJSON.Json.Serialize(requestParams);
                string jsonHeaders = MiniJSON.Json.Serialize(requestHeaders);
                string[] methodParams = { url, jsonParams, jsonHeaders, contentType };
                TreggHttpClient.BusAsyncCall("createPostRequest", methodParams, callback);
            }

            public static void CreateGetRequest(String url, Dictionary<string, string> requestParams,
                    Dictionary<string, string> requestHeaders, Action<Dictionary<string, object>> callback) {
                string jsonParams  = MiniJSON.Json.Serialize(requestParams);
                string jsonHeaders = MiniJSON.Json.Serialize(requestHeaders);
                string[] methodParams = { url, jsonParams, jsonHeaders };
                TreggHttpClient.BusAsyncCall("createGetRequest", methodParams, callback);
            }

            public static void CreatePutRequest(String url, Dictionary<string, string> requestParams,
                    Dictionary<string, string> requestHeaders, Action<Dictionary<string, object>> callback,
                    string contentType = "application/x-www-form-urlencoded") {
                string jsonParams  = MiniJSON.Json.Serialize(requestParams);
                string jsonHeaders = MiniJSON.Json.Serialize(requestHeaders);
                string[] methodParams = { url, jsonParams, jsonHeaders, contentType };
                TreggHttpClient.BusAsyncCall("createPutRequest", methodParams, callback);
            }

            public static void CreateDeleteRequest(String url, Dictionary<string, string> requestParams,
                    Dictionary<string, string> requestHeaders, Action<Dictionary<string, object>> callback) {
                string jsonParams  = MiniJSON.Json.Serialize(requestParams);
                string jsonHeaders = MiniJSON.Json.Serialize(requestHeaders);
                string[] methodParams = { url, jsonParams, jsonHeaders };
                TreggHttpClient.BusAsyncCall("createDeleteRequest", methodParams, callback);
            }

            private static void BusAsyncCall(string methodName, string[] methodParams, Action<Dictionary<string, object>> callback) 
			{
                Tregg.Bus.CallAsyncMethod("TreggHttpClientBus", methodName, methodParams, callback);
            }
        }
    }
}
